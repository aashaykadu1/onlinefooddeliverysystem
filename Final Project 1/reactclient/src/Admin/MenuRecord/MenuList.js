import axios from 'axios'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
// import { url } from '../../Constants/Url'
import MenuRow from './MenuRow'
import Navbar3 from '../../Components/A_navbar'
import { useNavigate } from 'react-router-dom';

const MenuList = () => {

  let navigate = useNavigate();
  
  const [menu, setMenu] = useState([])
  const [searchTerm, setsearchTerm]=useState("");

  useEffect(() => {
    console.log(`Menu is loaded`)
    getMenu()
  }, [])

  const getMenu = () => {
    axios.get('http://localhost:8080' + '/menu/all').then((response) => {
      const result = response.data
      if (result.status === 'success') {
        setMenu(result.data)
      } else {
        alert('error while loading list of menu')
      }
    })
  }

  const deleteMenu= async id => {
    await axios.delete('http://localhost:8080' + `/menu/delete/${menu.id}`)
    navigate('/MenuList')
    // getMenu()
  }

  return (

    <section class="pb-5 header text-center">
                  <Navbar3/>
    <div class="container py-5 text-white">
  
        <div class="row">
            <div class="col-lg-9 mx-auto">
                <div class="card border-0 shadow">
                    <div >

                        
                        <div >


                        <input type="text" placeholder="Search..." className="form-control"
                style={{marginTop:50, marginBottom:20, width:"40%"}}
            
                onChange ={(e)=>{

                        setsearchTerm(e.target.value);

                }}
            
            />





                            <table class="table m-0" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Menu_Id</th>
                                        <th scope="col">Menu Name</th>
                                        <th scope="col">price</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Description</th>
                            
                                    </tr>
                                </thead>
                                <tbody>
                                {/* {menu.map((menu) => {
                                   return <MenuRow menu={menu} />
                                 })} 
                                */}
                               
                               
                               
                                                  
                                  {menu.filter((val) => {

                    if ( searchTerm == ""){

                      return val;
                    }else if(

                      val.menuName.toLowerCase().includes(searchTerm.toLowerCase()) ||
                      // val.price.toLowerCase().includes(searchTerm.toLowerCase()) ||
                      val.description.toLowerCase().includes(searchTerm.toLowerCase())

                    ){
                      return val;
                    }
                    }).map((menu) => {
                                        return <MenuRow menu={menu} />
                                          })}

                               
                               
                               
                               
                               
                               
                               
                               
                               
                               
                               
                               
                               
                                </tbody>
                            </table>
                            <Link to="/AddMenuL">
                              <button type="button"  class="btn btn-success btn-sm">Add_Menu</button>
                            </Link>
                            {/* <a href="/HotelMenu" className="btn btn-info">Back</a> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


    // <div>
    //   <h1 className="page-title">Menu-List</h1>
    //   <Link to="/AddMenu">
    //     <a className="btn btn-success">Add Menu</a>
    //   </Link>
    //   <Link to="/delete-user">
    //     <a className="btn btn-success">Delete Menu</a>
    //   </Link>

    //   <table className="table table-striped">
    //     <thead>
    //       <tr>
    //         <th>id</th>
    //         <th>Menu Name</th>
    //         <th>Price</th>
    //         <th>Image_Name</th>
    //         <th>Description</th>
    //       </tr>
    //     </thead>
    //     <tbody>
          // {menu.map((menu) => {
          //   return <MenuRow menu={menu} />
          // })}
    //     </tbody>
    //   </table>
    //   <a href="/AdminHome" className="btn btn-info">Back</a>
    // </div>
  )
}

export default MenuList;
