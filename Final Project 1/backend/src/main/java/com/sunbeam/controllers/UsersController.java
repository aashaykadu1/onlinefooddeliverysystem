package com.sunbeam.controllers;





import java.util.HashMap;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.Credentials;
import com.sunbeam.dtos.Response;
import com.sunbeam.dtos.ResponseDto;
import com.sunbeam.dtos.UserDTO;
import com.sunbeam.entities.Users;
import com.sunbeam.services.UserService;
@CrossOrigin
@RestController
@RequestMapping("/user")
public class UsersController {
	@Autowired
	private UserService userService;
	
	@PostMapping("/login")
	public ResponseEntity<?> authenticateUser(@RequestBody Credentials loginDetails) {
		System.out.println(loginDetails);
		System.out.println("In Authenticate User " + loginDetails);
		System.out.println(loginDetails.getEmail());
		UserDTO userDto = userService.authenticate(loginDetails.getEmail(), loginDetails.getPassword());
		System.out.println(userDto);
		
		
		if(userDto==null)
			return new Response().error(userDto);
		return new ResponseEntity<>(new ResponseDto<UserDTO>("success",userDto),HttpStatus.CREATED);
	}
	
//	@PostMapping("/login")
//	public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest request) {
//		System.out.println("In Authenticate User " + request);
//		return new ResponseEntity<>(userService.authenticate(request.getEmail(), request.getPassword()),
//				HttpStatus.OK);
//	}
	
//	@PostMapping(value={"/signup"}, consumes = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<?> registerUser(@RequestBody User user){
//		user.setRole("customer");
//		System.out.println("in create new User"+user);
//		
//		return new ResponseEntity<>(new ResponseDto<Users>("success",userService.createNewUser(user)),HttpStatus.CREATED);
//	}
	
	@PostMapping("/signup")
	public ResponseEntity<?> signUp(@RequestBody UserDTO userDto){
		userDto.setRole("customer");
		System.out.println("in create new User"+userDto);
		UserDTO result = userService.save(userDto);
		//return new ResponseEntity<>(new ResponseDto<UserDTO>("success",userService.createNewUser(userDto)),HttpStatus.CREATED);
		return new ResponseEntity<>(new ResponseDto<UserDTO>("success",result),HttpStatus.CREATED);
		
	}

	@PostMapping(value={"/add_deliveryBoy"}, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> registerDeliveryBoy(@RequestBody UserDTO userDto){
		userDto.setRole("deliveryBoy");
		System.out.println("in create new User"+userDto);
		
		return new ResponseEntity<>(new ResponseDto<UserDTO>("success",userService.createNewUser(userDto)),HttpStatus.CREATED);
	}
	

	@PutMapping("/edit/{userId}")
	public ResponseEntity<?> updateProfile(@RequestBody Users user1 , @PathVariable int userId){
		user1.setId(userId);
		user1.setRole("customer");
		Users user = userService.update(user1);
		return ResponseEntity.ok(new ResponseDto<Users>("Success", user));
	}
	
	@GetMapping("/{userId}")
	public ResponseEntity<?> getUser( @PathVariable int userId){
		Users user = userService.findUser(userId);
		return ResponseEntity.ok(new ResponseDto<Users>("Success", user));
	}
	
//	@DeleteMapping("/delete/{userId}")
//	public ResponseEntity<?> deleteUser(@PathVariable int userId){
//		User user=userService.deleteById(userId);
//		return new ResponseEntity<>(new ResponseDto<User>("success",userService.deleteUserById(userId)),HttpStatus.OK));
//	}
	
	@PostMapping("/change_password")
	public ResponseEntity<?> changePassword(@RequestBody HashMap<String, String> map){
		String email = map.get("Email");
		String password = map.get("Password");
		
		return new ResponseEntity<>(new ResponseDto<>("success",userService.updatePassword(email , password)),HttpStatus.CREATED);
	}

	
	
	
	

	
}
