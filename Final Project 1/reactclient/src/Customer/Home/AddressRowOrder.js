import { Link, useNavigate } from 'react-router-dom'
import axios from 'axios'
// import { url } from '../../Constants/Url'

const AddressRowOrder = ({ addresse}) => {

  let navigate = useNavigate();
  const deleteAddress= async id => {
    await axios.delete('http://localhost:8080' + `/address/delete/${addresse.id}`)
    alert('successfully deleted an menu')
    navigate('/OrderDetails')
  }

  // sessionStorage.setItem("addressId",{addresse.});
  // console.log(sessionStorage.getItem('áddressId'))
  return (
    // sessionStorage.setItem("addressId",addresse.id);
    
    <tr>
      <td>{addresse.id}</td>
    
      <td>{addresse.addressLine1}</td>
      <td>{addresse.addressLine2}</td>
      <td>{addresse.city}</td>
      <td>{addresse.state}</td>
      <td>{addresse.country}</td>
      <td>{addresse.pinCode}</td>
     
     <td>   
       
        <Link to="/OrderDetails" class="btn btn-danger btn-sm" onClick={()=> deleteAddress(addresse.id)} >Delete
        </Link>      
    </td>
    </tr>
  )
}

export default AddressRowOrder;