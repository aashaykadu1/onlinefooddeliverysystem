package com.sunbeam.daos;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sunbeam.entities.OrderDetail;



public interface OrderDetailDao extends JpaRepository<OrderDetail, Integer>{

	@Query("Select od from OrderDetail od where od.currentOrder.id=:id")
	List<OrderDetail> findAllByOrderId(@Param("id") Integer id);

}

