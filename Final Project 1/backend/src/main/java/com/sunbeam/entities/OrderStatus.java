package com.sunbeam.entities;




public enum OrderStatus {
	PLACED,PACKING,READY,OUT_FOR_DELIVERY,DELIVERED,CANCELLED
}
