package com.sunbeam.services;



import java.util.List;
import java.util.Optional;

import com.sunbeam.entities.Cart;

public interface CartService {

	String addItemToCart(Integer productId, Integer quantity, Integer userId);

	List<Cart> getAllCartContents(Integer userId);

	String updateQuantity(Integer cartId, Integer quantity);
	
	Optional<Cart> findById(Integer cartId);

	void deleteFromCart(Integer cartId);

	void deleteAllFromCart(int userId);

	
}
