

### USERS

create table users 
(id int primary key auto_increment, 
first_name varchar(20), 
last_name varchar(20), 
email varchar(20), 
password varchar(20),
phone_no varchar(10),
role varchar (20));


insert into users values(1, 'Sanket', 'Kahalekar', 'sak@g.com', '123', '8793280246','customer');
insert into users values(default, 'Apoorv', 'Kahalekar', 'ak@g.com', '1234', '7276027760','admin');
insert into users values(default, 'john', 'cena', 'jc@g.com', '111', '8877665544','deliveryBoy');



### ADDRESS

create table addresses
(id int primary key auto_increment,
address_line_1 varchar(50),
address_line_2 varchar (50),
city varchar(30),
country varchar(30),
pin_code varchar (20),
state varchar (20),
user_id int,
FOREIGN KEY
(user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE CASCADE);




### ORDER

create table orders
(id int primary key auto_increment,
order_date datetime(6),
order_status varchar(30),
status_update_date datetime(6),
total_price double,
delivery_boy_id int not null,
user_id int not null,
delivery_addresses_id int not null,
CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES users(id),
CONSTRAINT fk_addresses FOREIGN KEY(delivery_addresses_id) REFERENCES addresses(id));



ALTER TABLE Orders
ADD FOREIGN KEY (delivery_boy_id) REFERENCES users(id);



CONSTRAINT fk_users FOREIGN KEY(delivery_boy_id) REFERENCES users(id),
CONSTRAINT fk_users FOREIGN KEY(user_id) REFERENCES users(id),


### MENU


create table menu
(id int primary key auto_increment,
menu_name varchar(20),
price double,
image_name varchar(20),
description varchar(50),
menu_type_id int, 
  FOREIGN KEY
(menu_type_id) REFERENCES menu_types(id) ON DELETE CASCADE ON UPDATE CASCADE);


insert into menu values(1, 'dosa', 25, 'dosa.img', 'favourate dish', 1);

insert into menu values(2, 'idli', 15, 'idli.img', 'cheapest dish', 1);

insert into menu values(3, 'andacurry', 60, 'anda.img', 'fav nonveg dish', 2);

insert into menu values(4, 'andabhurji', 40, 'bhurji.img', 'supb dish', 2);

insert into menu values(5, 'triplerice', 65, 'rice.img', 'supb dish', 3);

insert into menu values(6, 'shezwan_Noodles', 45, 'noodles.img', 'supb dish', 3);



### PAYMENT

create table payments
(id int primary key auto_increment,
amount double,
payment_date datetime(6),
status varchar(20),
type varchar(20),
order_id int not null,
FOREIGN KEY (order_id) REFERENCES orders(id));



### ORDER DETAILS

create table order_details
(id int primary key auto_increment,
price double,
quantity int,
order_id int not null,
menu_id int not null,
CONSTRAINT fk_orders FOREIGN KEY(order_id) REFERENCES orders(id),
CONSTRAINT fk_menus FOREIGN KEY(menu_id) REFERENCES menu(id));



### CART

create table cart
(id int primary key auto_increment,
quantity int,
menu_id int,
user_id int,
CONSTRAINT fk_menu FOREIGN KEY(menu_id) REFERENCES menu(id),
CONSTRAINT fk_users FOREIGN KEY(user_id) REFERENCES users(id));



/*  
FOREIGN KEY
(menu_id) REFERENCES menu(id),
FOREIGN KEY 
(user_id) REFERENCES users(id)

*/
create table cart
(id int primary key auto_increment,
quantity int,
menu_id int,
user_id int,

FOREIGN KEY (menu_id) REFERENCES menu(id),
FOREIGN KEY 
(user_id) REFERENCES users(id));




### MENU TYPE

create table menu_types
(id int primary key auto_increment,
menu_type varchar(20)
);


insert into menu_types values(1, 'veg');
insert into menu_types values(2, 'nonveg');
insert into menu_types values(3, 'chinese');
insert into menu_types values(4, 'southIndian');
insert into menu_types values(5, 'sweets');
insert into menu_types values(6, 'thali');
insert into menu_types values(7, 'rice');
insert into menu_types values(8, 'roti');
