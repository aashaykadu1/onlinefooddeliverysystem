import React from 'react';
import Ganesh from "../Admin/Images/Ganesh.jpg"
import Sanket from "../Admin/Images/Sanket.jpg"
import { Link } from 'react-router-dom';
import Aashay from "../Admin/Images/aashay.jpg"
import Shubham from "../Admin/Images/Shubham.jpeg"
const About=()=>{
    return(
   <section>
    <div className="container-fluid ">
    <div>
     <Link to="/customerHomePage">
     <button className="btn btn-dark"> Back To Home</button>
     </Link>
   </div>
    <div class="bg-light py-5">
      <div class="container py-5">
        <div class="row mb-4">
          <div>
            <h2 class="font-weight-light"><b>Foodies Team Work Efficiently </b></h2>
            <p class="font-italic text-muted">The Whole Project Leads That Youths..</p>
            <p>
            The platform and current demand for food allow many fresh apps to the food market, 
            also apps that are available hits success. Food is a most required thing, 
            this on-demand food delivery app provides delicious foods people ordered, 
            without making them skip a meal. The food sectors turn out mostly to an online platform with features on on-demand food delivery apps.
            </p>
          </div>
        </div>
        </div>
    
        <div class="row text-center">
         
          <div class="col-xl-3 col-sm-6 mb-5">
            <div class="bg-white rounded shadow-sm py-5 px-4"><img src={Ganesh} alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"/>
              <h5 class="mb-0">Ganesh Gaikwad</h5><span class="small text-uppercase text-muted">CEO - Founder</span>
            
            </div>
          </div>
         
          <div class="col-xl-3 col-sm-6 mb-5">
            <div class="bg-white rounded shadow-sm py-5 px-4"><img src={Sanket} alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"/>
              <h5 class="mb-0">Sanket Kahalekar</h5><span class="small text-uppercase text-muted">CEO - Founder</span>
             
            </div>
          </div>
          
          <div class="col-xl-3 col-sm-6 mb-5">
            <div class="bg-white rounded shadow-sm py-5 px-4"><img src={Shubham} alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm" />
              <h5 class="mb-0">Shubham Jadhav</h5><span class="small text-uppercase text-muted">CEO - Founder</span>
             
            </div>
          </div>
         
          <div class="col-xl-3 col-sm-6 mb-5">
            <div class="bg-white rounded shadow-sm py-5 px-4"><img src={Aashay} alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm" />
              <h5 class="mb-0">Aashay kadu</h5><span class="small text-uppercase text-muted">CEO - Founder</span>
              
            </div>
          </div>
          
    
        </div>
      </div>
    </div>
   
    
    
    <footer class="bg-light pb-5">
      <div class="container text-center">
        <p class="font-italic text-muted mb-0">&copy; Copyrights Foodies.com All rights reserved.</p>
      </div>
    </footer>
    </section>

    )
}
export default About;