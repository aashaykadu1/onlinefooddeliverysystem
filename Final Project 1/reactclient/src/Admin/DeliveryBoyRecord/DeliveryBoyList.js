import axios from 'axios'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
// import { url } from '../../Constants/Url'
//  import DeliveryBoyRow from '../DeliveryBoyRecord/DeliveryBoyRow'
import DeliveryBoyRow1 from './DeliveryBoyRow1'
import Navbar3 from '../../Components/A_navbar'

const DeliveryBoyList = () => {
  
  const [deliveryBoyList, setDeliveryBoyList] = useState([])
  const [searchTerm, setsearchTerm]=useState("");

  useEffect(() => {
    console.log(` is loaded`)
    getDeliveryBoyList()
  }, [])

  const getDeliveryBoyList = () => {
    axios.get( 'http://localhost:8080/admin/deliveryBoyList').then((response) => {
      const result = response.data
      if (result.status === 'success') {
        setDeliveryBoyList(result.data)
      } else {
        alert('error while loading list of DeliveryBoyList')
      }
    })
  }

  return (

    <section class="pb-5 header text-center" >
      <Navbar3/>
    <div class="container py-5 text-white" >
        <div class="row">


            <div class="col-lg-9 mx-auto">
                <div class="card border-0 shadow">
                    <div >

                        
                        <div >


                        <input type="text" placeholder="Search..." className="form-control"
                style={{marginTop:50, marginBottom:20, width:"40%"}}
            
                onChange ={(e)=>{

                        setsearchTerm(e.target.value);

                }}
            
            />


                            <table class="table m-0" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">DeliveryBoy_Id</th>
                                        <th scope="col">FirstName</th>
                                        <th scope="col">LastName</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">PhoneNo</th>
                            
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                      
                                    {/* {deliveryBoyList.map((user) => {
                                       return <DeliveryBoyRow1 user={user} />
                                      })} */}

                                       





              {deliveryBoyList.filter((val) => {

                if ( searchTerm == ""){

                  return val;
                }else if(

                  val.firstName.toLowerCase().includes(searchTerm.toLowerCase()) ||
                  val.lastName.toLowerCase().includes(searchTerm.toLowerCase()) ||
                  val.email.toLowerCase().includes(searchTerm.toLowerCase())

                ){
                  return val;
              }
              }).map((user) => {
                                       return <DeliveryBoyRow1 user={user} />
                                      })}


{/* {MockData.filter((val) => {

if (searchTerm == ""){

    return val;

}else if(

    val.first_name.toLowerCase().includes(searchTerm.toLowerCase()) ||
    val.last_name.toLowerCase().includes(searchTerm.toLowerCase()) ||
    val.email.toLowerCase().includes(searchTerm.toLowerCase())
){
    return val;
}
}).map((m) => (

<tr key={m.id}>
    <td>{m.first_name}</td>
    <td>{m.last_name}</td>
    <td>{m.email}</td>
    <td>{m.gender}</td>


</tr>


)) }


 */}






                                    

                                  
                                   
                                </tbody>
                            </table>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
        <Link to="/AddDeliveryBoy">
       <button type="button"  class="btn btn-success btn-sm">Add DeliveryBoy</button>
       </Link>
      
</section>
  )
}

export default DeliveryBoyList;
