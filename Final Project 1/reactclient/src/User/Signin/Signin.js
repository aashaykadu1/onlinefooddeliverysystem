import axios from 'axios'
import { useState } from "react";
import { useNavigate } from 'react-router-dom';
// import SigninImg from '../Customer/Images/scooter.gif'
// import SigninImg from '../Images/login.webp'
 import { toast } from 'react-toastify';
 import "./Signin.css";
 



 const Signin = () => {
  const navigate = useNavigate();
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const signinUser = () => {
        if (email.length == 0) {
           
             toast.warning('please enter email')
        } else if (password.length === 0) {
            
             toast.warning('please enter password')
  } else {
           
            const body = new FormData()
            body.append('email', email)
           
            body.append('password', password)
          

            axios.post('http://localhost:8080/user/login',{'email':email,'password':password} , {"Content-Type": "application/json"}).then((response) => {
              

              //  console.log(response);
                const result = response.data
              
                if (result.status === 'success' ) {
                    console.log("login Successfull!!!!!")
                    sessionStorage.setItem("userId" , result.data.id)
                    sessionStorage.setItem("firstName" , result.data.firstName)
                    console.log(result.data.firstName)
                    if (result.data.role === "customer") {
    
                        navigate('/CustomerHomePage')
                        toast.success('Welcome to foodies!!! enjoy every bite...')
                    }
                    else if (result.data.role === "admin") {
                        navigate('/userlist')
                        toast.success("Welcome ")
                    }
                   else if (result.data.role === "deliveryBoy") {
                    sessionStorage.setItem("deliveryId" , result.data.id)
                    console.log(result.data.id)
                        navigate('/DeliveryBoyHomePage')
                        toast.success('Come on!!! Your reward is waiting for U')
                        
                    }
                   

                     
                } else {
                  toast.error('invalid username or password')
                  navigate('/signin')
                  console.log('invalid')
                    
                }
            }
            )
        }


    }
  
    return (

      <div class="login-box">  
        <h2>Login</h2>  
        <form>  
       <div class="user-box">  
        <input type="email" name="" required="" 
        onChange={(e)=>{
                                setEmail(e.target.value);
                            }} />  
        <label>Email</label>  
       </div>  
       <div class="user-box">  
        <input type="password" name="" required=""
        onChange={(e)=>{
                               setPassword(e.target.value);
                                }}/>  
        <label>Password</label>  
       </div>  
      
        
        {/* <button onClick={signinUser}  >Login</button> */}
        

       <div className="pt-1 mb-4">
                         <button onClick={signinUser} className="btn btn-success" type="button">Login</button>
                       </div>
    
                       <a className="small text-muted" href="forgotPassword">Forgot password?</a>
                       <p className="mb-5 pb-lg-2" >Don't have an account? <a href="signup">Register here</a></p>
        </form>  
     </div> 


      
   



   )
  
}

export default Signin;
