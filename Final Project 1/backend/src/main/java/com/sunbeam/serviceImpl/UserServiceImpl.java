package com.sunbeam.serviceImpl;


import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.sunbeam.daos.UsersDao;
import com.sunbeam.dtos.Credentials;
import com.sunbeam.dtos.DtoEntityConverter;
import com.sunbeam.dtos.UserDTO;
import com.sunbeam.entities.Users;
import com.sunbeam.services.UserService;

import ch.qos.logback.core.pattern.Converter;

@Transactional
@Service
public class UserServiceImpl implements UserService{
	@Autowired
	private UsersDao userdao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private DtoEntityConverter converter;

	public UserDTO findByEmail(String email) {
		Users user = userdao.findByEmail(email);
		return converter.toUserDto(user); 
	}
	
	public UserDTO findbyEmailAndPassword(Credentials cred) {
		Users dbUser = userdao.findByEmail(cred.getEmail());
		String rawPassword = cred.getPassword();
		if(dbUser != null && passwordEncoder.matches(rawPassword, dbUser.getPassword())) {
			UserDTO result = converter.toUserDto(dbUser);
			result.setPassword("*********");
			return result;
		}
		return null;
	}
@Override
	public UserDTO save(UserDTO userDto) {
		String rawPassword = userDto.getPassword();
		String encPassword = passwordEncoder.encode(rawPassword);
		userDto.setPassword(encPassword);
		Users user = converter.toUserEntity(userDto);
		user = userdao.save(user);
		userDto = converter.toUserDto(user);
		userDto.setPassword("******");
		return userDto;
	}

	@Override
	public UserDTO authenticate(String email, String password) {
		UserDTO u=findByEmail(email);
				if(u!=null && u.getPassword().equals(password))
					return u;
		return null;
	}

	@Override
	public Users update(Users u) {
		return userdao.save(u);
	}

	@Override
	public boolean deleteById(int id) {
		if(userdao.existsById(id)) {
			userdao.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public List<Users> findAll() {
		return userdao.findAll();
	}

	@Override
	public Users finById(int uid) {
		return userdao.getById(uid);
	}

	@Override
	public void deleteAll() {
	
		
	}

	@Override
	public UserDTO createNewUser(UserDTO userDto) {
		return userdao.save(userDto);
	}	
	
	public List<Users> getAllUser(String role) {
		return userdao.findByRole(role);
	}

	@Override
	public List<Users> getAllDeliveryBoy(String role) {
		
		return userdao.findByRole(role);
	}

	@Override
	public Users findUser(int userId) {
		Optional<Users> user = userdao.findById(userId);
		return user.orElse(null);
	}

	@Override
	public String updatePassword(String email, String password) {
		Users user = userdao.findByEmail(email);
		 user.setPassword(password);
		return "password Changed Successfully!!!";

	}

	



}
