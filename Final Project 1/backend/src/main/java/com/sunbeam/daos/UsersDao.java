package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.dtos.UserDTO;
import com.sunbeam.entities.Users;

public interface UsersDao extends JpaRepository<Users, Integer> {
	
	
//	Users findById(int id);
	
	Users findByEmailAndPassword(String email, String password);
	
	Users findByEmail(String email);
	
	List<Users> findByRole(String role);

	UserDTO save(UserDTO userDto);
	

}
