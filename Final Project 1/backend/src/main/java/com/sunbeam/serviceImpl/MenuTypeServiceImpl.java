package com.sunbeam.serviceImpl;


import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunbeam.daos.Menu_TypeDao;
import com.sunbeam.entities.Menu;
import com.sunbeam.entities.MenuType;
import com.sunbeam.services.MenuTypeService;

@Transactional
@Service
public class MenuTypeServiceImpl implements MenuTypeService{
	

	@Autowired
	private Menu_TypeDao menutypedao;
	@Override
	public List<MenuType> getAllMenuTypes() {
		return menutypedao.findAll();			
	}
	
	@Override
	public MenuType addOrEditCategory(MenuType menuType1) {
		
		return menutypedao.save(menuType1);
	}
	@Override
	public List<MenuType> findAll() {
		return menutypedao.findAll();
	}

	@Override
	public Menu addMenu(Menu menu, int id) {
		Optional<MenuType> menuType = menutypedao.findById(id);
		if(menuType.isPresent()) {
			menuType.get().addMenu(menu);
			return menu;
		}
		return null;
	}

	@Override
	public MenuType findMenuType(int id) {
		Optional<MenuType> menutype = menutypedao.findById(id);
		return menutype.orElse(null);
	}

	@Override
	public List<Menu> findByMenuType(int id) {
		Optional<MenuType> menuType1 = menutypedao.findById(id);
		MenuType menutype = menuType1.orElse(null);
		List<Menu> menulist = menutype.getMenuList();
		return menulist;
	}


	
	
	
}
