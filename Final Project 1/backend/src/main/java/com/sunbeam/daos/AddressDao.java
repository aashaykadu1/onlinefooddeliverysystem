package com.sunbeam.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sunbeam.entities.Address;



public interface AddressDao  extends JpaRepository<Address, Integer>{
	
//	@Query("Select a from Address a join fetch a.selectedUser where a.selectedUser.id=:id")
//	@Query("Select a from Address a  where a.user_id=:id")
	List<Address> getAllAddressesByUserId(@Param("id") Integer userId);

}
