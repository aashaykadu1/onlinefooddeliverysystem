
import axios from 'axios'
import { useState,useEffect, createContext } from 'react'
import { Link, useNavigate } from 'react-router-dom'
// import signupImg from '../Images/signup.jpg'
import "./Signup.css";
import { toast } from 'react-toastify';
import Navbar2 from '../../Components/C_navbar';


const LoggedUser = createContext();

const Signup = () => {
  
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [phoneNo, setPhoneNo] = useState('')
//   let [data , setData] = useState(undefined)

const [user, setUser] = useState([])
  
   const navigate = useNavigate()






  
  
  
  
  const SignupUser= (e) => {

     e.preventDefault();






    if (firstName.length == 0) {
        toast.warning('Please enter first name')
      } else if (lastName.length == 0) {
        toast.warning('Please enter last name')
      } else if (email.length == 0) {
        toast.warning('Please enter email')
      } else if (password.length == 0) {
        toast.warning('Please enter password')
      } else if (phoneNo.length == 0) {
        toast.warning('Please enter your mob no')
      }  else {
        const body = {
          firstName,
          lastName,
          email,
          password,
          phoneNo,
        }
  
        // url to call the api
        const url = `http://localhost:8080/user/signup`
  
        // http method: post
        // body: contains the data to be sent to the API
        axios.post(url, body).then((response) => {
          // get the data from the response
          const result = response.data
          console.log(result)
          if (result['status'] == 'success') {
            toast.success('Successfully signed up new user')
  
            // navigate to the signin page
            navigate('/signin')
          } else {
            toast.error(result['error'])
          }
        })
      }


























}

  
    return (

      <>
      <LoggedUser.Provider value={ firstName }>

      </LoggedUser.Provider>
      
      <section className="container-fluid bg">
          
      
      <div class="login-box">  
        <h2>Sign up</h2>  
        <form>  
       <div class="user-box">  
        <input type="text" name="" required="" 
         onChange={(e)=>{
                         setFirstName(e.target.value);
                     }} />  
        <label>First Name</label> 
        </div> 

        <div class="user-box">  
        <input type="text" name="" required="" 
         onChange={(e)=>{
                              setLastName(e.target.value);
                              }} />  
        <label>Last Name</label>  
       </div>

        <div class="user-box">  
        <input type="email" name="" required="" 
        onChange={(e)=>{
                              setEmail(e.target.value);
                              }} />  
        <label>Email</label>  
       </div> 

       <div class="user-box">  
        <input type="password" name="" required=""
         onChange={(e)=>{
                              setPassword(e.target.value);
                              }}  />  
        <label>Password</label>  
       </div> 

       <div class="user-box">  
        <input type="text" name="" required="" 
        onChange={(e)=>{
                              setPhoneNo(e.target.value);
                              }} />  
        <label>Mobile No</label> 
        </div>

        {/* <a href="#">   */}
         
        <button onClick={SignupUser} type="submit" className="btn btn-primary btn-block">Submit</button>
       {/* </a>   */}
       <div className="mb-3 form-check">
       

         <Link to="/signin">
                         <button className="btn btn-dark ">Back to login</button>
                         </Link>
      </div>
       {/* <Link to="/login" > </Link> */}
         
                 
       </form>
       </div>

       </section>
     

     </>


    //  <section className="container-fluid mg">
        

    //     <section className="row justify-content-center">
    //         <section className="col-12 col-sm-6 col-md-3">


    //             <form className="form-container">
    //                 <div className="mb-3">
    //                     <h1>Signup Here</h1>
    //                     <br/>
    //                   <label for="exampleInputEmail1" className="form-label">First Name</label>
    //                   <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
    //                   name= "firstName"

    //                    onChange={(e)=>{
    //                setFirstName(e.target.value);
    //            }}
                      
    //                   />
                     

    //                 </div>





    //                 <div className="mb-3">
    //                   <label for="exampleInputPassword1" className="form-label">Last Name</label>
    //                   <input type="text" className="form-control" id="exampleInputPassword1"
    //                   name= "lastName"
                      
    //                   onChange={(e)=>{
    //                     setLastName(e.target.value);
    //                     }}
                      
                      
    //                  />
    //                 </div>









    //                 <div className="mb-3">
    //                   <label for="exampleInputPassword1" className="form-label">Email</label>
    //                   <input type="email" className="form-control" id="exampleInputPassword1"
    //                   name= "email"
                      
    //                   onChange={(e)=>{
    //                     setEmail(e.target.value);
    //                     }}
                      
                      
    //                  />
    //                 </div>








    //                 <div className="mb-3">
    //                   <label for="exampleInputPassword1" className="form-label">Password</label>
    //                   <input type="password" className="form-control" id="exampleInputPassword1"
    //                   name= "password"
                      
    //                   onChange={(e)=>{
    //                     setPassword(e.target.value);
    //                     }}
                      
                      
    //                  />
    //                 </div>





    //                 <div className="mb-3">
    //                   <label for="exampleInputPassword1" className="form-label">Phone No</label>
    //                   <input type="text" className="form-control" id="exampleInputPassword1"
    //                   name= "phoneNo"
                      
    //                   onChange={(e)=>{
    //                     setPhoneNo(e.target.value);
    //                     }}
                      
                      
    //                  />
    //                 </div>









    //                 <div className="mb-3 form-check">
    //                   {/* <input type="checkbox" className="form-check-input" id="exampleCheck1"/> */}
    //                   {/* <label className="form-check-label" for="exampleCheck1">Check me out</label> */}
                    
                    
    //                   <a className="small text-muted" href="forgotPassword">Forgot password?</a>
    //                  <br />

    //                 </div>

    //                 <Link to="/login" >

    //                 <button 
                    
    //                 onClick={SignupfirstName} type="submit" className="btn btn-primary btn-block">Submit</button>
                 
    //              </Link>

                 
                    
    //                 <div className="col-sm-6">

    //                         <br />

    //                     <Link to="/signin3">
    //                     <button className="btn btn-dark ">Back to login</button>
    //                     </Link>
    //              </div>
                 
                 
                 
    //               </form>





    //         </section>


    //     </section>


    // </section>
       


   )
  
}

export default Signup;
export { LoggedUser };
