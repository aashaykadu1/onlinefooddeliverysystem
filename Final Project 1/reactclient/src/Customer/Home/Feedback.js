import React, { useContext } from 'react'
import emailjs from 'emailjs-com'
import { useState } from 'react'
import { toast } from 'react-toastify'
import { Link } from 'react-router-dom'


const Feedback = () => {

       

    const [user_name, setUser_name] = useState('');
    const [user_email, setUser_email] = useState('');
    const [message, setMessage] = useState('');


    const sendEmail=(e)=>{

        e.preventDefault();

        
        if (user_name.length === 0) {
            // alert('please enter name')
            toast.warning('Please enter name')
        } else if (user_email.length === 0) {
            // alert('Enter email')
            toast.warning('Please enter your email')
        }else if (message.length === 0){
            // alert('enter your message')
            toast.warning('Please enter your message')
        }else
       {



        


        emailjs.sendForm('service_akxvcz2', 'template_pydi0w2', e.target, 'bkhGBMysTR-3vsrDq'
        
        
        ).then(res=>{

            console.log(res);
            // alert("your feedback successfully submitted")
            toast.success('We have successfully registered your feedback')


        }).catch(err=> console.log(err));

        e.target.reset()


     }

    }



  return (
    <div className='container border'
    
    style={{marginTop:"50px",
    width:'50%',
     backgroundImage:`url('https://img.freepik.com/free-photo/hand-painted-watercolor-background-with-sky-clouds-shape_24972-1095.jpg?size=626&ext=jpg')`,
//    backgroundColor: 'orange',
    backgroundPosition:"center",
    backgroundSize:"cover",

}}
    
    
    >

        <h1 style={{marginTop:'25px'}}>Ping your Feedback </h1>

        <form className="row" style={{ margin:"25px 85px 75px 100px"}} 
        onSubmit={sendEmail}>

        <label>Name</label>
        <input type='text' name='user_name' className="form-control"
        
        onChange={(e)=>{
            setUser_name(e.target.value);
        }}
        
        
        />
        


        <label>Email</label>
        <input type='email' name='user_email' className="form-control"
        
        onChange={(e)=>{
            setUser_email(e.target.value);
        }}
        
        
        />
        
        


        <label>Message</label>
        <textarea name='message' rows='4' className="form-control"
        
        onChange={(e)=>{
            setMessage(e.target.value);
        }}
        
        
        />


            <input type='submit' value='Send' 
            className="form-control btn btn-primary"
            style={{marginTop:"30px"}}
            
            />



<div className="mb-3 mt-5 form-check">
       

       <Link to="/CustomerHomePage">
                       <button className="btn btn-dark ">Back to Home</button>
                       </Link>
    </div>


        </form>
      
    </div>
  )
}

export default Feedback;
