// import { url } from '../../Constants/Url'
import { Link, useNavigate } from 'react-router-dom'

const OrdersRow = ({ order }) => {
  console.log(order)
  const navigate=useNavigate()
  const AssignOrder=()=>{
    sessionStorage.setItem('order_Id',order.order.id)
    sessionStorage.setItem('order_status',order.order.orderStatus)
    navigate('/AssignOrder')
  }
  return (
    <tr>
      {/* <td>{order.order.customer.id}</td> */}
      <td>{order.order.id}</td>
      {/* <td>{order.orderDetails.selectedMenu.menuName}</td> */}
      {/* <td>{order.orderDetails.quantity}</td> */}
     
      <td>
        {order.order.customer.firstName}   {order.order.customer.lastName}  {order.order.customer.phoneNo}
         
         
         
      </td>
      <td> {order.order.deliveryAddress.addressLine1} ,
      {order.order.deliveryAddress.addressLine2},
      {order.order.deliveryAddress.city},
      {order.order.deliveryAddress.pinCode}.
      </td>
      <td>{order.payment.amount}</td>
      <td>{order.order.orderStatus}</td>
      <td>{order.payment.status}</td>
      <td>     
      {/* <Link to="/AssignOrder"> */}
      <button type="button" onClick={AssignOrder} class="btn btn-dark btn-sm">Select Order</button>
        {/* </Link>     */}
     </td>
    </tr>
  )
}

export default OrdersRow;
