package com.sunbeam.services;



import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.sunbeam.entities.Menu;
import com.sunbeam.dtos.MenuDto;

public interface MenuService {

	List<Menu> findAll();
	String deleteMenu(int id);
	Menu editMenu(MenuDto menuDto, int id);
	Menu addMenu(Menu menu, MultipartFile multipartFile, int menuTypeid);
	Menu saveMenu(Menu menu, MultipartFile imageName);
//	
}
