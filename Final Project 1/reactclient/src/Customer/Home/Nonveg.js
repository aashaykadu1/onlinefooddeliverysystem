import axios from 'axios'
import { useState, useEffect } from 'react'
// import { Link } from 'react-router-dom'
// import { url } from '../../Constants/Url'
// import MenuTypeRow from './MenuTypeRow'
import Navbar2 from '../../Components/C_navbar'
import MenuTypeRow1 from './MenuTypeRow1'
import { Row, Col } from "react-bootstrap";

const Nonveg = () => {
  const [menuTypeId,setMenuTypeId]=useState(2)
  const [menutype, setMenuType] = useState([])

  useEffect(() => {
    console.log(`menutype is loaded`)
    getMenutype()
  }, [])

  const getMenutype = () => {
    axios.get('http://localhost:8080' + `/menutype/allMenuByType/${menuTypeId}`).then((response) => {
      const result = response.data
      if (result.status === 'success') {
        setMenuType(result.data)
      } else {
        alert('error while loading list of menutype')
      }
    })
  }

  return (

    <>
    <Navbar2/>
    <Row>
              {menutype.map((menutype) => (
                <Col key={menutype.menuTypeId} md={3}>
                  <MenuTypeRow1 menutype={menutype} />
                </Col>
              ))}
            </Row>
                 
    
               
    </>


    // <div>
    //   <h1 className="page-title">Menu-List</h1>
    //   <Link to="/AddMenu">
    //     <a className="btn btn-success">Add Menu</a>
    //   </Link>
    //   <Link to="/delete-user">
    //     <a className="btn btn-success">Delete Menu</a>
    //   </Link>

    //   <table className="table table-striped">
    //     <thead>
    //       <tr>
    //         <th>id</th>
    //         <th>Menu Name</th>
    //         <th>Price</th>
    //         <th>Image_Name</th>
    //         <th>Description</th>
    //       </tr>
    //     </thead>
    //     <tbody>
          // {menu.map((menu) => {
          //   return <MenuRow menu={menu} />
          // })}
    //     </tbody>
    //   </table>
    //   <a href="/AdminHome" className="btn btn-info">Back</a>
    // </div>
  )
}

export default Nonveg;



// import React from "react";
// import Navbar2 from "../../Components/C_navbar";
// import { Link } from "react-router-dom";
// import chikencurry from '../Images/Nonveg/chikencurry.jpg'
// import kabab from '../Images/Nonveg/kabab.jfif'
// import MurghMusallam from '../Images/Nonveg/MurghMusallam.jpg'

// const Nonveg=()=>{
//     return(
//         <div className="container">
//         <Navbar2></Navbar2>
//         <div className="container">
        
//         <div className="container">

//             <table>
//                 <tr>
//                     <td>
//                         <div className=" anchordiv" >
//                             <Link className="nav-link" to="/addcart">
//                                 <div >
//                                     <img src={chikencurry} className="anchorimages "  />
//                                 </div>
                            
                               
//                             </Link>
//                         </div>
//                        <a href="/addcart" className="btn btn-success">addcart</a>
//                     </td>

//                     <td>
//                         <div className="anchordiv">
//                             <Link className="nav-link" to="/addcart">
//                                 <div>
//                                     <img src={kabab} className="anchorimages" />
//                                 </div>
//                                 <h6>kabab</h6>
//                             </Link>
//                         </div>
                        
//                     </td>
//                     <td>
//                         <div className="anchordiv">
//                             <Link className="nav-link" to="/addcart">
//                                 <div>
//                                     <img src={MurghMusallam} className="anchorimages" />
//                                 </div>
//                                 <h6>MurghMusallam</h6>
//                             </Link>
//                         </div>
                        
//                     </td>
//                 </tr>
               
//             </table>

//         </div>
//     </div>

//  </div >
//     )
// }
// export default Nonveg;