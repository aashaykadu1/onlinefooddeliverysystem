package com.sunbeam.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



//+------------+-------------+------+-----+---------+----------------+
//| Field      | Type        | Null | Key | Default | Extra          |
//+------------+-------------+------+-----+---------+----------------+
//| id         | int         | NO   | PRI | NULL    | auto_increment |
//| first_name | varchar(20) | YES  |     | NULL    |                |
//| last_name  | varchar(20) | YES  |     | NULL    |                |
//| email      | varchar(20) | YES  | UNI | NULL    |                |
//| password   | varchar(20) | YES  |     | NULL    |                |
//| phone_no   | varchar(10) | YES  |     | NULL    |                |
//| role       | varchar(20) | YES  |     | NULL    |                |
//+------------+-------------+------+-----+---------+----------------+



@Entity
@Table(name = "users")
public class Users {

	@Id
	@Column(name = "id")
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "email")
	private String email;
	@Column(name = "password")
	private String password;
	@Column(name = "phone_no")
	private String phoneNo;
	@Column(name = "role")
	//@JsonIgnore
	private String role;
	
	
	
	
	@OneToMany(mappedBy = "user" ,cascade = {CascadeType.ALL},orphanRemoval = true)
	private List<Address> addressList = new ArrayList<>();
	
	
	
	
	
	public Users() {
		
	}

public Users(int id) {
	this.id = id;
		
	}

	public Users(int id, String firstName, String lastName, String email, String password, String phoneNo,
			String role) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phoneNo = phoneNo;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
	public List<Address> getAddressList() {
		return addressList;
	}

	public void setAddressList(List<Address> addressList) {
		this.addressList = addressList;
	}
	
	public void addAddress(Address address) {
		this.addressList.add(address);
		address.setUser(this);
	}
	
	public void removeUser(Address address) {
		this.addressList.remove(address);
		address.setUser(null);
	}

	@Override
	public String toString() {
		return "Users [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", password=" + password + ", phoneNo=" + phoneNo + ", role=" + role + "]";
	}
	
	
	
}
