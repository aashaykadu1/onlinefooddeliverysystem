package com.sunbeam.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "addresses")
//@JsonIgnoreProperties
public class Address extends BaseEntity{
	
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Id
//	private int id;
	
	
	
	
	
//	+----------------+-------------+------+-----+---------+----------------+
//	| Field          | Type        | Null | Key | Default | Extra          |
//	+----------------+-------------+------+-----+---------+----------------+
//	| id             | int         | NO   | PRI | NULL    | auto_increment |
//	| address_line_1 | varchar(50) | YES  |     | NULL    |                |
//	| address_line_2 | varchar(50) | YES  |     | NULL    |                |
//	| city           | varchar(30) | YES  |     | NULL    |                |
//	| country        | varchar(30) | YES  |     | NULL    |                |
//	| pin_code       | varchar(20) | YES  |     | NULL    |                |
//	| state          | varchar(20) | YES  |     | NULL    |                |
//	| user_id        | int         | YES  | MUL | NULL    |                |
//	+----------------+-------------+------+-----+---------+----------------+
	
	
	
	@Column(name = "address_line_1")
	private String addressLine1;
	@Column(name = "address_line_2")
	private String addressLine2;
	@Column(name = "city")
	private String city;
	@Column(name = "country")
	private String Country;
	@Column(name = "pin_code")
	private String pinCode;
	@Column(name = "state")
	private String state;
	
	@ManyToOne
	@JoinColumn(name = "user_id" ,nullable = false)
	@JsonIgnore
	private Users user;

	public Address() {
		super();
	}

	public Address(String addressLine1, String addressLine2, String city, String country, String pinCode, String state,
			Users user) {
		
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		Country = country;
		this.pinCode = pinCode;
		this.state = state;
		this.user = user;
	}

	public Address(int id,String addressLine1, String addressLine2, String city, String country, String pinCode,
			String state) {
		
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		Country = country;
		this.pinCode = pinCode;
		this.state = state;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	
	public Users getUser() {
		return user;
	}
	
	public void setUser(Users user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Address [addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", city=" + city
				+ ", Country=" + Country + ", pinCode=" + pinCode + ", state=" + state  + "]";
	}
	
	
	

}
