import axios from 'axios'
import { useEffect, useState } from "react";
import { useNavigate, useParams } from 'react-router-dom';


const AddCart=(props)=>{
   const {id} = useParams();
// console.log(props.match.params)
    console.log(id)
    let navigate = useNavigate();
    const [menuId,setMenuId] = useState('')
    const [quantity, setQuantity] = useState('')
    const [userId,setUserId]=useState('')
    const [cart]=useState([])

    useEffect(() => {
      // console.log(`menutype is loaded`)
      // getMenutype()
      
      let id=sessionStorage.getItem("menuId");
      let id1=sessionStorage.getItem("userId");
    setMenuId(id);
    setUserId(id1);

      
    }, [])
    
    const setCart = () => {
        if (menuId.length === 0) {
            alert('please enter menuId')
        } else if (quantity.length === 0) {
            alert('Enter new quantity')
        } else {
           
            const body = new FormData()
            body.append('menuId', menuId)
           
            body.append('quantity', quantity)
            body.append('userId', userId)
         

            axios.post('http://localhost:8080/cart/add',{'menuId':menuId,'quantity':quantity,'userId':userId} , {"Content-Type": "application/json"}).then((response) => {
               

               console.log(response);
                const result = response.data
                // sessionStorage.setItem("userId" , result.data.id)
                if (result.status === 'success') {
                    
                    alert("Your cart Added Successfully !!!")
    
                        navigate('/AddAddress')       
                } else {
                    alert('cart Failed')
                }
            }
            )
        }


    }
    return(
        <section className="vh-100" >
        <div className="container py-5 h-100">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col col-xl-10">
              <div className="card">
                <div className="row g-0">
                  <div className="flex align-items-center">
                    <div className="card-body p-4 p-lg-5 text-black">
      
                      <form class="mx-1 mx-md-4">
                        <h2 className="fw-normal mb-3 pb-3"><b>Your Cart</b></h2>

                        <div className="form-outline mb-4">
                          <input type="text" id="form2Example27" class="form-control form-control-lg" value={userId}
                           onChange={(e)=>{
                           setUserId(e.target.value);
                            }}/>
                          <label className="form-label" for="form2Example27" 
                          ><b>UserId</b></label>
                        </div>

                        <div className="form-outline mb-4">
                          <input className="text" id="form2Example17" class="form-control form-control-lg" value={menuId}
                          onChange={(e)=>{
                            setMenuId(e.target.value);
                        }} />
                          <label className="form-label" for="form2Example17" 
                                  ><b>MenuId</b></label>
                        </div>
                        
      
                        <div className="form-outline mb-4">
                          <input type="number" id="form2Example27" class="form-control form-control-lg" 
                           onChange={(e)=>{
                           setQuantity(e.target.value);
                            }}/>
                          <label className="form-label" for="form2Example27" 
                          ><b>Quantity</b></label>
                        </div>
                        
      
                        <div className="pt-1 mb-4">
                          <button onClick={setCart} className="btn btn-success" type="button">Save</button>
                        </div>
                      </form>
      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
}

export default AddCart;

// import axios from 'axios'
// import { useState,useEffect } from 'react'
// import { Link, usenavigate } from 'react-router-dom'
// import CartRow from "./CartRow"
// import { url } from '../../Constants/Url'
// import Navbar2 from "../../Components/C_navbar";

// const AddCart=()=>{
//     const [userid]=useState(sessionStorage.getItem("userid"))
//     const [cart, setCart] = useState([])

//   useEffect(() => {
   
//     console.log(`Cart is loaded`)
//     getCart()

    
//   }, [])
//   const getCart = () => {
//     // const id = sessionStorage.getItem("userId");
//     // axios.get(url + `/cart/getcart/${userid}`).then((response) => {
//       axios.get(url + `/cart/all/${userid}`).then((response) => {
//       const result = response.data
//       if (result.status === 'success') {
//         setCart(result.data)
//       } else {
//         alert('error while loading list of cart')
//         // alert('Your cart Is Loaded')
//       }
//     })
//   }


//     return(
//         <section class="pb-5 header text-center">
       
// <div class="container py-5 text-white">
// <Navbar2></Navbar2>
// <div class="row">
//   <div class="col-lg-9 mx-auto" class="myStyle">
//       <div class="card border-0 shadow">
//           <div >
//               <div >
//                   <table class="table m-0">
//                       <thead>
//                           <tr>
//                               <th scope="col">cart_Id</th>
//                               <th scope="col">MenuType</th>
    
//                               <th scope="col">Menu Name</th>
//                               <th scope="col">Qty</th>
//                               <th scope="col">Price</th>
                             
//                           </tr>
                          
//                       </thead>
//                       <tbody>
//                                  {cart.map((cart) => {
//                                    return <CartRow cart={cart} />
//                                  })}
                      
//                     </tbody>
//                   </table>
                 
//                                    <Link to="/OrderDetails">
//                                    <button type="button"  class="btn btn-success btn-sm">Placed Order</button>
//                                     </Link>
                            
//               </div>
//           </div>
//       </div>
//   </div>
// </div>
// </div>
// </section>
//     )
// }

// export default AddCart