import { Link } from "react-router-dom"
import '../Components/navbar.css'
import { useState } from "react"

const Navbar4=()=>{
  const [firstName]=useState(sessionStorage.getItem('firstName'))
    return(
        <nav class="navbar navbar-expand-lg py-3 navbar-dark bg-dark shadow-sm">
  <div class="container">
    <a href="#" class="navbar-brand" >
      <span class="text-uppercase font-weight-bold">Delivery  Panel  !!!</span>
    </a>
    <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler"><span class="navbar-toggler-icon"></span></button>

    <div id="navbarSupportedContent" class="collapse navbar-collapse">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item"><a href="/AllOrdersRecord" class="nav-link">Current Orders </a></li>
        <li class="nav-item"><a href="/MyAllOrders" class="nav-link">My Assigned Order</a></li>
       
        <div className="btn-group" class="mybtn-right">
  <button type="button" class="btn btn-dark dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
  <i class="fas fa-user"></i>
          
          &nbsp; Welcome { firstName }
         
  </button>
  <ul class="dropdown-menu dropdown-menu-end">
  
  <li><a class="dropdown-item" href="/UpdateProfile">Update Profile</a></li>
  <li><a class="dropdown-item" href="/signin">Log Out</a></li>
    
    
  </ul>
</div>
      </ul>
    </div>
  </div>
</nav>
    )
}

export default Navbar4;