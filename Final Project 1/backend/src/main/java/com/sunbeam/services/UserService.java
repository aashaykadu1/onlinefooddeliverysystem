package com.sunbeam.services;



import java.util.List;

import com.sunbeam.dtos.UserDTO;
import com.sunbeam.entities.Users;

public interface UserService {
	
	UserDTO findByEmail(String email);
//	Users save(Users u);
	UserDTO authenticate(String email,String password);
	Users update(Users u);
	boolean deleteById(int id);
	List<Users> findAll();
	Users finById(int uid);
	void deleteAll();
	UserDTO createNewUser(UserDTO userDto);

	List<Users> getAllUser(String role);
	List<Users> getAllDeliveryBoy(String role);
	Users findUser(int userId);
	String updatePassword(String email, String password);
	UserDTO save(UserDTO userDto);

	  
}
