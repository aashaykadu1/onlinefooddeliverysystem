import axios from 'axios';
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

const CartRow = ({ cart }) => {

  const navigate=useNavigate()
  const deleteCart =()=>{
    axios.delete('http://localhost:8080' + `/cart/delete/${cart.id}`)
    toast.warning('Item removed from cart')
    navigate('/OrderDetails')

  }



  return (
    
    
    <tr>
        
      {/* <td>{cart.id}</td> */}
      <td>{cart.selectedMenu.menutype.menuType}</td>
     
      <td>{cart.selectedMenu.menuName}</td>
      <td>{cart.quantity}</td>
      <td>{cart.selectedMenu.price}</td>
      <td>{cart.selectedMenu.price * cart.quantity}</td>
    
      
     <td>   
     <Link to="/OrderDetails">
     <button type="button" class="btn btn-danger btn-sm" onClick={()=>deleteCart(cart.id)}>Delete</button>
        </Link>      
    </td>
    </tr>
    
    


  )
}

export default CartRow;