package com.sunbeam.services;


import java.util.List;

import com.sunbeam.entities.Menu;
import com.sunbeam.entities.MenuType;

public interface MenuTypeService {
	List<MenuType> getAllMenuTypes();

	MenuType addOrEditCategory(MenuType menuType);

	List<MenuType> findAll();

	Menu addMenu(Menu menu, int id);

	MenuType findMenuType(int id);

	List<Menu> findByMenuType(int id);

	
}
 