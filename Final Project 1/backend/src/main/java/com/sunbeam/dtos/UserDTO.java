package com.sunbeam.dtos;

import org.springframework.beans.BeanUtils;


import com.sunbeam.entities.Users;

public class UserDTO {
	
	private int id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String role;
	private String phoneNo;
	
	public UserDTO() {
		
	}

	






	public UserDTO(int id, String firstName, String lastName, String email, String password, String role,
			String phoneNo) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.role = role;
		this.phoneNo = phoneNo;
	}








	public int getId() {
		return id;
	}








	public void setId(int id) {
		this.id = id;
	}








	public String getFirstName() {
		return firstName;
	}








	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}








	public String getLastName() {
		return lastName;
	}








	public void setLastName(String lastName) {
		this.lastName = lastName;
	}








	public String getEmail() {
		return email;
	}








	public void setEmail(String email) {
		this.email = email;
	}








	public String getPassword() {
		return password;
	}








	public void setPassword(String password) {
		this.password = password;
	}








	public String getRole() {
		return role;
	}








	public void setRole(String role) {
		this.role = role;
	}








	public String getPhoneNo() {
		return phoneNo;
	}








	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}








	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", password=" + password + ", role=" + role + ", phoneNo=" + phoneNo + "]";
	}



	public static UserDTO fromEntity(Users entity) {
		UserDTO dto=new UserDTO();
		BeanUtils.copyProperties(entity, dto);
		return dto;
		
	}





}
