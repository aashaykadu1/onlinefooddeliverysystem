import { Link } from "react-router-dom"
import AddressRowOrder from "./AddressRowOrder"
import CartRowOrder from "./CartRowOrder"
import axios from 'axios'
import { useEffect } from 'react'
// import { url } from '../../Constants/Url'
import { useState } from "react"
import Navbar2 from "../../Components/C_navbar"
// import ShowCart from "../../Customer/Cart/ShowCart"

const OrderDetails=()=>{
  let price= 0;
  const [userid]=useState(sessionStorage.getItem('userId'))
  const [cart, setCart] = useState([])

  // const [addressId,setAddressId] =useState(sessionStorage.getItem('addressId'));
  const [addresse , setAddresse] = useState([])
  useEffect(() => {
    console.log(`User is loaded`)
    getAddresse()
    console.log(`Cart is loaded`)
    getCart()
  }, [])

  const deleteAddress= async id => {
    await axios.delete('http://localhost:8080' + `/address/delete/${addresse.id}`)
    alert('successfully deleted an menu')
  }

  const getAddresse = () => {
    // axios.get('http://localhost:8080' + `/address/find/${userid}`).then((response) => {
    axios.get('http://localhost:8080' + `/address/all/${userid}`).then((response) => {
      const result = response.data
      console.log(result.data.addressId)
      sessionStorage.setItem("addressId",result.data.addressId);
      if (result.status === 'success') {
        setAddresse(result.data)
      } else {
        alert('error while loading list of users')
      }
    })
  }
  const getCart = () => {
    // const id = sessionStorage.getItem("userId");
    // axios.get(url + `/cart/getcart/${userid}`).then((response) => {
  // const [userid]=useState(1)
      axios.get('http://localhost:8080' + `/cart/all/${userid}`).then((response) => {
      const result = response.data
      if (result.status === 'success') {
        setCart(result.data)
      } else {
        alert('error while loading list of cart')
        // alert('Your cart Is Loaded')
      }
    })
  }

    return(
      // <div>

      
      //   <h3>we are in Order_details </h3>
      //   <Link to="/address">
      //                           <button type="button"  class="btn btn-info btn-sm">Change Address</button>
      //                       </Link>
      //   </div>
        

      

        <section className="h-100 bg-dark">
          <Navbar2/>
         
            <div className="row d-flex justify-content-center align-items-center h-100">
              <div className="col">
                 <div className="card card-registration my-4">
                  <div className="row g-0">
                   <div className="col-xl-6 d-none d-xl-block">
                     <h3 className="mb-5 text-uppercase">Your Orders</h3>
                    
                        <section class="pb-5 header text-center">
       
                            <div class="container py-5 text-white">
       
                                 <div class="row">
                                   <div class="col-lg-9 mx-auto">
                                     <div class="card border-0 shadow">
                                       <div >
                                         <div >
                                         <table class="table m-0">
                      <thead>
                          <tr>
                              <th scope="col">cart_Id</th>
                              <th scope="col">MenuType</th>
    
                              <th scope="col">Menu Name</th>
                              <th scope="col">Qty</th>
                              <th scope="col">Price</th>
                              <th scope="col">Total Price</th>
                             
                          </tr>
                          
                      </thead>
                      <tbody>
                        {
                          cart.forEach((cart)=> {
                           
                                  price=price + (cart.selectedMenu.price * cart.quantity)
                                  console.log(price)
                          })
                        }
                                 {cart.map((cart) => {
                                  //  console.log(cart.selectedMenu.price)
                                  
                                   return <CartRowOrder cart={cart} />
                                 })}
                      
                    </tbody>

                    {/* <tfoot>
                      <tr>
                      <b>Grand Total = {price}</b>
                        <th></th>
                      </tr>
                    </tfoot>  */}

                    <tr >
                      <td  colspan="8" >
                        <b>Grand Total = {price} </b>
                        
                      </td>
                    </tr>
                  </table>

                 

                  <Link to="/CustomerHomePage">
     <button type="button" class="btn btn-danger btn-sm">Back</button>
        </Link>






                                           {/* <ShowCart></ShowCart> */}
                                           {/* <table class="table m-0">
                                             <thead>
                                                 <tr>
                                                   <th scope="col">Order_Id</th>
                                                   <th scope="col">Menu Name</th>
                                                   <th scope="col">Qty</th>
                                                   <th scope="col">Price</th>
                                                 </tr>
                                 
                                             </thead>
                                             <tbody>
                                               <tr>
                                               <td>1</td>
                                               <td>Panner Masala</td>
                                               <td>2</td>
                                               <td>240 Rs</td>
                                               <h3>Total Price _240Rs_</h3>
                                               </tr>
                                             </tbody>
                                           </table> */}
                        
                                          </div>
                                       </div>
                                     </div>
                                    </div>
                                   </div>
                                  </div>
                           </section>
                          </div>

                           <div className="col-xl-6">
                           <div className="card-body p-md-5 text-black">
                              <h3 className="mb-5 text-uppercase">Your Address</h3>
                             <section class="pb-5 header text-center">

       <section>
         
         <div>
         <table class="table m-0">
                         <thead>
                                 <tr>
                                 <th scope="col">Id</th>
                                     <th scope="col">AddresLine</th>
                                     <th scope="col">Address Line 2</th>
                                     <th scope="col">City</th>
                                     <th scope="col">State</th>
                                     <th scope="col">Country</th>
                                     <th scope="col">pinCode</th>

                                 </tr>
                                 
                             </thead>
                             <tbody>

                             <tr>
      <td>{addresse.id}</td>
    
      <td>{addresse.addressLine1}</td>
      <td>{addresse.addressLine2}</td>
      <td>{addresse.city}</td>
      <td>{addresse.state}</td>
      <td>{addresse.country}</td>
      <td>{addresse.pinCode}</td>
     
     <td>   
       
        {/* <Link to="" class="btn btn-danger btn-sm" onClick={()=> deleteAddress(addresse.id)} >Delete
        </Link>       */}
    </td>
    </tr>
                             {addresse.map((addresse) => {
                               sessionStorage.setItem("addressId",addresse.id)
                                return <AddressRowOrder addresse={addresse} />
                                 })}
                           </tbody>
                         </table>
         </div>
       </section>
       
                            <Link to="/Payment">
                               <button type="button"  class="btn btn-success btn-sm">Confirm Order</button>
                            </Link>
                            <Link to="/AddAddress">
                               <button type="button"  class="btn btn-info btn-sm">Add Address</button>
                            </Link>
       </section>
                

                
               

                
               
                
               
                
               

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
 
</section>

    )
}

export default OrderDetails

// <div className="container py-5 h-100">
// <div className="row d-flex justify-content-center align-items-center h-100">
//   <div className="col">
//      <div className="card card-registration my-4">
//       <div className="row g-0">
//        <div className="col-xl-6 d-none d-xl-block">

// <table class="table m-0">
// <thead>
//         <tr>
//             <th scope="col">AddresLine</th>
//             <th scope="col">Address Line 2</th>
//             <th scope="col">City</th>
//             <th scope="col">State</th>
//             <th scope="col">Country</th>
//             <th scope="col">pinCode</th>

//         </tr>
        
//     </thead>
//     <tbody>
//     {addresse.map((addresse) => {
//        return <AddressRow addresse={addresse} />
//         })}
//   </tbody>
// </table>
              
// </div>
//                  </div>
//              </div>
             
             
//          </div>
//        </div>
//        </div>
//        </section>