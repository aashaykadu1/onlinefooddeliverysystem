package com.sunbeam.dtos;

import org.springframework.stereotype.Component;

import com.sunbeam.entities.Users;

@Component
public class DtoEntityConverter {
	public UserDTO toUserDto(Users entity) {
		UserDTO dto = new UserDTO();
		dto.setFirstName(entity.getFirstName());
		dto.setLastName(entity.getLastName());
		dto.setEmail(entity.getEmail());
		dto.setPassword(entity.getPassword());
		dto.setRole(entity.getRole());
		dto.setPhoneNo(entity.getPhoneNo());
		return dto;
	}
	
	public Users toUserEntity(UserDTO dto) {
		Users entity = new Users();
		entity.setFirstName(dto.getFirstName());
		entity.setLastName(dto.getLastName());
		entity.setEmail(dto.getEmail());
		entity.setPassword(dto.getPassword());
		entity.setRole(dto.getRole());
		entity.setPhoneNo(dto.getPhoneNo());
		return entity;
	}

}
