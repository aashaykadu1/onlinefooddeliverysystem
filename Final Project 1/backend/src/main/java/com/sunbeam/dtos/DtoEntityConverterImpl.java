package com.sunbeam.dtos;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.sunbeam.entities.Menu;

@Component
public class DtoEntityConverterImpl {

	public Menu menuFormDtoToEntity(AddMenuInDto addmenuDto) {
		Menu menu=new Menu();
		BeanUtils.copyProperties(addmenuDto, menu, "imageName");
		
		if(addmenuDto.getImageName()!= null)
			menu.setImageName(addmenuDto.getImageName().getOriginalFilename());
		return menu;
	}
	
	
	}

