package com.sunbeam.controllers;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sunbeam.dtos.ResponseDto;
import com.sunbeam.entities.Menu;
import com.sunbeam.entities.MenuType;
import com.sunbeam.services.MenuTypeService;

@CrossOrigin
@Controller
@RequestMapping("/menutype")
public class MenuTypeController {
	
	@Autowired
	private MenuTypeService menuTypeService;
	
	@PostMapping("/add")
	public ResponseEntity<?> addNewMenuType(@RequestBody MenuType menuType){
		MenuType menuType1 = menuTypeService.addOrEditCategory(menuType);
		return new ResponseEntity<>(new ResponseDto<MenuType>("success", menuType1),HttpStatus.CREATED);
	}
	
//	@GetMapping("/all")
//	public ResponseEntity<?> getAllMenuTypes(){
//		List<MenuType> menuTypeList = menuTypeService.findAll();
//		System.out.println(menuTypeList);
//		return new ResponseEntity<>(new ResponseDto<List<MenuType>>("success", menuTypeList),HttpStatus.CREATED);
//	}
	
	@GetMapping("/all")
	public ResponseEntity<?> findAllMenuType() {
		List<MenuType> list = menuTypeService.findAll();
//		List<MenuType> result = new ArrayList<MenuType>();
		return ResponseDto.success(list);
	}
	
	@GetMapping("/allMenuByType/{id}")
	public ResponseEntity<?> menuByType(@PathVariable int id){
		List<Menu> menu = menuTypeService.findByMenuType(id);
		return new ResponseEntity<>(new ResponseDto<List<Menu>>("success", menu),HttpStatus.CREATED);
	}

	



	
}
