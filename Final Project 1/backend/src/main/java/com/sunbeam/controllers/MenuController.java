package com.sunbeam.controllers;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeam.dtos.AddMenuDto;
import com.sunbeam.dtos.AddMenuInDto;
import com.sunbeam.dtos.MenuDto;
import com.sunbeam.dtos.ResponseDto;
import com.sunbeam.entities.Menu;
import com.sunbeam.entities.MenuType;
import com.sunbeam.services.MenuService;
import com.sunbeam.services.MenuTypeService;

@CrossOrigin 
@RestController
@RequestMapping("/menu")
public class MenuController {
	@Autowired
	private MenuService menuService;
	@Autowired
	private MenuTypeService menuTypeService;
	
//	@PostMapping("/addmenu")
//	public ResponseEntity<?>addNewMenu(@RequestBody Menu menu){
//		System.out.println(menu);
//		return new ResponseEntity<>(menuService.addnewMenu(menu),HttpStatus.OK);
//	}
	
	// successfully working but menutype goes null value
	
//	@PostMapping("/addMenu")
//	public ResponseEntity<?> save(AddMenuInDto addMenuInDto) {
//		MenuType menuType=menuTypeService.findMenuType(addMenuInDto.getId());
//		Menu menu1 = new Menu();
//		Menu menu = AddMenuInDto.toEntity(addMenuInDto);
//		menu = menuService.saveMenu(menu,addMenuInDto.getImageName());
//		return ResponseDto.success(menu);
//	}
	
	
	//uncomment
	@PostMapping("/addMenu/{menuTypeid}")
	public ResponseEntity<?> save(AddMenuInDto addMenuInDto) {
		MenuType menuType=menuTypeService.findMenuType(addMenuInDto.getId());
		Menu menu1 = new Menu();
		Menu menu = AddMenuInDto.toEntity(addMenuInDto);
		menu = menuService.saveMenu(menu,addMenuInDto.getImageName());
		return ResponseDto.success(menu);
	}
//	
	

//	@PostMapping("/add/{id}")
//	public ResponseEntity<?> addMenu(@RequestBody Menu menu ,  @PathVariable int id){
//		System.out.println(menu);
//		Menu menu1 = menuTypeService.addMenu(menu, id);
//		System.out.println(menu1);
//		return new ResponseEntity<>(new ResponseDto<Menu>("Success" , menu1 ),HttpStatus.CREATED);
//	}
	
	//uncomment
	@PostMapping("/add/{menuTypeid}")
	   public ResponseEntity<?> addMenu(AddMenuInDto menuInDto ,  @PathVariable int menuTypeid){
		System.out.println(menuTypeid);
	       Menu menu = AddMenuInDto.toEntity(menuInDto);
	       menu = menuService.addMenu(menu , menuInDto.getImageName() ,menuTypeid );
	       return new ResponseEntity<>(new ResponseDto<Menu>("Success" , menu),HttpStatus.CREATED);
	   }
	
	
//	@GetMapping("/all")
//	public ResponseEntity<?> getAllMenuTypes(){
//		List<Menu> menuList = menuService.findAll();
//		System.out.println(menuList);
//		return new ResponseEntity<>(new ResponseDto<List<Menu>>("success", menuList),HttpStatus.CREATED);
//	}
	
	// successfully working
	// all menu for admin
	@GetMapping("/all")
	public ResponseEntity<?> findAllMenu() {
		List<Menu> list = menuService.findAll();
		//Stream<ArtistDTO> result = list.stream().map(artist -> ArtistDTO.fromEntity(artist));
		List<AddMenuDto> result = new ArrayList<AddMenuDto>();
		for (Menu menu : list)
			result.add( AddMenuDto.fromEntity(menu) );
		return ResponseDto.success(result);
	}
	
	
	
	// done
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteMenu(@PathVariable int id){
		String message = menuService.deleteMenu(id);
		return new ResponseEntity<>(new ResponseDto<>("success", message),HttpStatus.CREATED);
	}
	
	@PutMapping("/edit/{id}")
	public ResponseEntity<?> editMenu(@RequestBody MenuDto menuDto , @PathVariable int id){
		System.out.println(menuDto);
		System.out.println(id);
		Menu menu = menuService.editMenu(menuDto , id);
//		System.out.println("----------"+ menu+ "---------------");
//		String message = "success";
		return new ResponseEntity<>(new ResponseDto<>("Success" ,menu ),HttpStatus.OK);
	}


	
	
//	@GetMapping("/findmenu")
//	public ResponseEntity<?>findAllMenu(){
//		System.out.println("All menu");
//		return new ResponseEntity<>(menuService.getAllMenu(),HttpStatus.OK);
//	}

}
