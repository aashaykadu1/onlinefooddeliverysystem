package com.sunbeam.services;

import java.util.List;

import com.sunbeam.entities.Address;

public interface AddressService {

	Address addAddress(Address address, int userId);

	Address editAddress(Address address, int addressId);

	String deleteAddress(int addressId);

	List<Address> getAllAddressesByUserId(int userId);

}
