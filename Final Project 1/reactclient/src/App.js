// import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes, Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'


import CustomerHome from './Customer/Home/CustomerHomePage';

import Veg from './Customer/Home/Veg';


import Feedback from './Customer/Home/Feedback';

import Contact from './Common/ContactUs';
import About from './Common/AboutUs';

import AdminHome from './Admin/Home/AdminHomePage';
import DeliveryHome from './Delivery/DeliveryBoyHomePage';
import Header from './Common/Header';

import AddMenu from './Admin/MenuRecord/AddMenuL'; 
import AddMenuType from './Admin/MenuRecord/AddMenuType';
import MenuType from './Admin/MenuRecord/MenuTypeList'
import MenuTypeRow from './Customer/Home/MenuTypeRow';
import EditMenu from './Admin/MenuRecord/EditMenu';
import MenuList from './Admin/MenuRecord/MenuList';
import HotelMenu from './Admin/MenuRecord/HotelMenu';

import AllOrders from './Admin/Home/AllOrders'
import MyOrders from './Customer/Home/MyOrders';

 import Signin  from './User/Signin/Signin';


import Signup from './User/Signup/Signup';





import Forgotpassword from './User/Forgotpassword/Forgotpassword';
import UserList from './Admin/UserRecord/UserList';

import DeliveryBoyList from './Admin/DeliveryBoyRecord/DeliveryBoyList';
import UpdateProfile from './User/Updateprofile/UpdateProfile';
import Cart from './Customer/Home/Cart';
import AddCart from './Customer/Home/AddCart';
import AddAddress from './Customer/Home/AddAddress';
import OrderDetails from './Customer/Home/OrderDetails';
import Payment from './Customer/Home/Payment';
import VisitAgain from './Customer/Home/VisitAgain';
import AllOrdersRecord from './Delivery/AllOrdersRecord';
import AssignOrder from './Delivery/AssignOrder';
import AddDeliveryBoy from './Admin/DeliveryBoyRecord/AddDeliveryBoy';
import MyAllOrders from './Delivery/MyAllOrders'


import Veg1 from './Customer/Home/Veg1';
import Thali from './Customer/Home/Thalis';
import South from './Customer/Home/South';

import { createContext, useContext } from 'react';
// import { LoggedUser } from './User/Signin/Signin';
import Nonveg from './Customer/Home/Nonveg';
import Chineese from './Customer/Home/Chineese';
const value = createContext();

function App() {

  // const email = useContext(LoggedUser);

  // console.log(email);
  return (

   
    // <value.Provider value={"sanket"}>


    
    
    <div>
    <BrowserRouter>
    <div className="App">
     {/* <h1>React starts...</h1> */}
     
    
      

     <Routes>
     
     <Route exact path='/' element={<Header/>}/>
     <Route exact path='/signin' element={<Signin/>}/>
    
     
     


     <Route exact path='/signup' element={<Signup/>}/> 
     
     
     <Route exact path='/contact' element={<Contact/>}/>
     <Route exact path='/about' element={<About/>}/>


     <Route exact path='/forgotpassword' element={<Forgotpassword/>}/>

     
     <Route exact path='/AddMenuType' element={<AddMenuType />} />
     <Route exact path='/AddMenuL' element={<AddMenu />} />
     <Route exact path='/MenuTypeRow' element={<MenuTypeRow/>}/>
     <Route exact path='/MenuTypeList' element={<MenuType/>}/>
     <Route exact path='/EditMenu' element={<EditMenu/>}/>
     <Route exact path='/HotelMenu' element={<HotelMenu/>}/>

     <Route exact path='/AllOrders' element={<AllOrders/>}/>
     <Route exact path='/MyOrders' element={<MyOrders/>}/>



     




     <Route exact path='/CustomerHomePage' element={<CustomerHome/>}/>
     {/* <Route exact path='/Veg' element={<Veg/>}/> */}
     <Route exact path='/Veg1' element={<Veg1/>}/>
     <Route exact path='/Thalis' element={<Thali/>}/>
     <Route exact path='/SouthIndian' element={<South/>}/>


     <Route exact path='/Nonveg' element={<Nonveg/>}/>
     <Route exact path='/Chineese' element={<Chineese/>}/>

     <Route exact path='/updateprofile' element={<UpdateProfile/>}/>
     <Route exact path='/Cart' element={<Cart/>}/>
     <Route exact path='/AddCart' element={<AddCart/>}/>
     <Route exact path='/AddAddress' element={<AddAddress/>}/>
     <Route exact path='/OrderDetails' element={<OrderDetails/>}/>
     <Route exact path='/Payment' element={<Payment/>}/>
     <Route exact path='/VisitAgain' element={<VisitAgain/>}/>
     
     
     
     <Route exact path='/Feedback' element={<Feedback/>}/>



     <Route exact path='/AdminHomePage' element={<AdminHome/>}/> 
     <Route exact path='/UserList' element={<UserList/>}/> 
     <Route exact path='/MenuList' element={<MenuList/>}/> 
     <Route exact path='/DeliveryBoyList' element={<DeliveryBoyList/>}/> 
     <Route exact path='/AddDeliveryBoy' element={<AddDeliveryBoy/>}/> 

     <Route exact path='/DeliveryBoyHomePage' element={<DeliveryHome/>}/>
     <Route exact path='/AllOrdersRecord' element={<AllOrdersRecord/>}/>
     <Route exact path='/AssignOrder' element={<AssignOrder/>}/>MyAllOrders
     <Route exact path='/MyAllOrders' element={<MyAllOrders/>}/>


     </Routes>



    </div>
    </BrowserRouter>
    <ToastContainer theme="colored" />
    </div>
    // </value.Provider>
  );
}

export default App;
export { value };
