package com.sunbeam.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sunbeam.entities.MenuType;

public interface Menu_TypeDao extends JpaRepository<MenuType ,Integer> {

}