package com.sunbeam.serviceImpl;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunbeam.daos.AddressDao;
import com.sunbeam.daos.CartDao;
import com.sunbeam.daos.OrderDao;
import com.sunbeam.daos.OrderDetailDao;
import com.sunbeam.daos.PaymentDao;
import com.sunbeam.daos.UsersDao;
import com.sunbeam.dtos.OrderResponse;
import com.sunbeam.entities.Address;
import com.sunbeam.entities.Cart;
import com.sunbeam.entities.Order;
import com.sunbeam.entities.Type;
import com.sunbeam.entities.OrderDetail;
import com.sunbeam.entities.OrderStatus;
import com.sunbeam.entities.Payment;
import com.sunbeam.entities.PaymentStatus;
import com.sunbeam.entities.Users;
import com.sunbeam.services.OrderService;



@Service
@Transactional
public class OrderServiceImpl implements OrderService {
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private CartDao cartDao;
	
	@Autowired
	private AddressDao addressDao;
	
	@Autowired
	private UsersDao userDao;
	
	@Autowired
	private PaymentDao paymentDao;
	
	@Autowired 
	private OrderDetailDao orderDetailDao;

	@Override
	public String placeOrderForUser(int userId, int addrId,String paymentMode) {
		// get all cart items for given user
		List<Cart> cartItems = cartDao.findAllItemsByUser(userId);
		
		double total = 0.0;
		int deliveryCharges = 25;
		for (Cart item : cartItems) {
			total += item.getQuantity() * item.getSelectedMenu().getPrice();
		}

		Address address = addressDao.findById(addrId).get();
		Users customer = userDao.findById(userId).get();
		
		System.out.println("-------------------------------------");
		Order newOrder = new Order(total, LocalDateTime.now(), OrderStatus.PLACED, LocalDateTime.now(), address, customer,null);
		orderDao.save(newOrder);

		
		Payment payment = new Payment(total + deliveryCharges, LocalDateTime.now(), paymentMode.equals("COD") ? PaymentStatus.PENDING : PaymentStatus.COMPLETED, Type.valueOf(paymentMode), newOrder);
		paymentDao.save(payment);
		cartItems.forEach(item -> {
			orderDetailDao.save(new OrderDetail(item.getSelectedMenu().getPrice(), item.getQuantity(), newOrder,
					item.getSelectedMenu()));
		});
		cartDao.deleteAll(cartItems);
		return "Order Placed Successfully!";
	}

	@Override
	public List<OrderResponse> getAllOrders() {
		List<Order> orders = orderDao.findAll();
		List<OrderResponse> response = new ArrayList<>();
		
		for (Order order : orders) {
			List<OrderDetail> orderDetails =  orderDetailDao.findAllByOrderId(order.getId());
			Payment payment = paymentDao.findPaymentByOrderId(order.getId());
			response.add(new OrderResponse(order, orderDetails,payment));
		}
		return response;
	}

	
	
	@Override
	public List<OrderResponse> getAllPendingOrders() {
		List<Order> orders = orderDao.findAll();
		List<OrderResponse> response = new ArrayList<>();
		
		for (Order order : orders) {
			List<OrderDetail> orderDetails =  orderDetailDao.findAllByOrderId(order.getId());

					String s=order.getOrderStatus().toString();

			if( s.equals("OUT_FOR_DELIVERY") || s.equals("PLACED")) {
				Payment payment = paymentDao.findPaymentByOrderId(order.getId());
				response.add(new OrderResponse(order, orderDetails,payment));
			}
			System.out.println(response);
		}
		return response;
	}
	
	
	@Override
	public List<OrderResponse> getAllAssignedOrders(int userId) {
		List<Order> orders = orderDao.findAllOrdersByEmployeeId(userId);
		
		List<OrderResponse> response = new ArrayList<>();
		
		for (Order order : orders) {
			List<OrderDetail> orderDetails =  orderDetailDao.findAllByOrderId(order.getId());
			Payment payment = paymentDao.findPaymentByOrderId(order.getId());
			response.add(new OrderResponse(order, orderDetails,payment));
		}
		return response;
	}
	
	
	
	public List<OrderResponse> getMyOrders(int userId){
		List<Order> orders = orderDao.findAllOrdersByUserId(userId);
		System.out.println(orders+" ");
List<OrderResponse> response = new ArrayList<>();
		
		for (Order order : orders) {
			List<OrderDetail> orderDetails =  orderDetailDao.findAllByOrderId(order.getId());
			Payment payment = paymentDao.findPaymentByOrderId(order.getId());
			response.add(new OrderResponse(order, orderDetails,payment));
		}
		return response;
	}
	
	//new try
	


	@Override
	public List<OrderResponse> getAllCustomerOrders(int userId) {
		List<Order> orders = orderDao.findAllOrdersByUserId(userId);
		
		List<OrderResponse> response = new ArrayList<>();
		
		for (Order order : orders) {
			List<OrderDetail> orderDetails =  orderDetailDao.findAllByOrderId(order.getId());
			Payment payment = paymentDao.findPaymentByOrderId(order.getId());
			response.add(new OrderResponse(order, orderDetails,payment));
		}
		return response;
	}
	
	
	@Override
	public void assignDeliveryBoy(int userId, int orderId) {
		Order order = orderDao.findById(orderId).get();
		Users user = userDao.findById(userId).get();
		order.setDeliveryBoy(user);
		order.setOrderStatus(OrderStatus.OUT_FOR_DELIVERY);
		return;
	}

	@Override
	public void updateOrderStatus(int orderId, String status ,int deliveryId) {
		Order order = orderDao.findById(orderId).get();
		order.setOrderStatus(OrderStatus.valueOf(status));
		order.setStatusUpdateDate(LocalDateTime.now());
		order.setDeliveryBoy(new Users(deliveryId));
		if(status.equals("DELIVERED")) {
			Payment payment = paymentDao.findPaymentByOrderId(orderId);
			payment.setStatus(PaymentStatus.COMPLETED);
		}
	return;
	
	}

}
