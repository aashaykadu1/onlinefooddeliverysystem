// import { url } from '../../Constants/Url'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { useNavigate } from 'react-router-dom';
import Ganesh from "../../Admin/Images/d.jpg"
// import Menu from './MenuList'

const MenuRow = ({ menu }) => {

  console.log(menu.imageName);
  let navigate = useNavigate();

  const deleteMenu= async id => {
    await axios.delete('http://localhost:8080' + `/menu/delete/${menu.id}`)
    navigate('/MenuList')
    // getMenu()
  }
  return (
    <tr>
      <td>{menu.id}</td>
      <td>{menu.menuName}</td>
      <td>
        {menu.price}
      </td>
      <td> 
        <img width="100px"height="100px"
          src={'http://localhost:8080' + '/images/' + menu.imageName}
          // src={'http://localhost:8080' + '/' + menu.imageName}
          // src={Ganesh}
          // src={'C:/Users/gaikw/OneDrive/Desktop/New folder/ganesh/reactclient/files'+ '/' +menu.imageName}
          alt=""
          className="imageName-sm"
        />
      </td>
     
      <td>{menu.description}</td>
      
      <td>     
      {/* <Link to="/EditMenu">
      <button type="button" class="btn btn-dark btn-sm">Edit</button>
        </Link>     */}
     
     </td>
     <td>   
     <Link to="" class="btn btn-danger btn-sm" onClick={()=> deleteMenu(menu.id)} >Delete
        </Link>      
    </td>
    </tr>
  )
}

export default MenuRow;
