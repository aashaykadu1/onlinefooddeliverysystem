import React from "react";
import { Link } from "react-router-dom";

const Contact=()=>{
    return(
        <section>
        <div className="container">
        <div>
          <Link to="/CustomerHomePage">
             <button className="btn btn-dark">Back To Home</button>
         </Link>
        </div>
        <div class="bg-light py-5">
          <div>
            <h2><b>CONTACT US...</b></h2>
          </div>
        
            <div class="row text-center">
             
              <div class="col-xl-3 col-sm-6 mb-5">
                <div class="bg-white rounded shadow-sm py-5 px-4"><img src="" alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"/>
                  <h5 class="mb-0">Pune ShivajiNagar</h5><h6>Ph. 000000</h6><span class="small text-uppercase text-muted">Contact No : 7387774573 Address :CJ Heights,Sadguru Corner,Shivajinagar, Pune : 415517 </span>
                  <ul class="social mb-0 list-inline mt-3">
                   
                  </ul>
                </div>
              </div>
              <div class="col-xl-3 col-sm-6 mb-5">
                <div class="bg-white rounded shadow-sm py-5 px-4"><img src="" alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"/>
                  <h5 class="mb-0">Pimpri-Chinchawad </h5> <h6>Ph. 111111</h6>
                  <span class="small text-uppercase text-muted">Address :Food park,Dange Chowk,Pimpri-chinchwad: 415011 </span>
                  <ul class="social mb-0 list-inline mt-3">
                    
                  </ul>
                </div>
              </div>
              <div class="col-xl-3 col-sm-6 mb-5">
                <div class="bg-white rounded shadow-sm py-5 px-4"><img src="" alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"/>
                  <h5 class="mb-0">Pune Kothrud</h5><h6>Ph.22222</h6><span class="small text-uppercase text-muted"> Address :Candent Heights,Chandani-chowk,Kothrud, Pune : 415510 </span>
                  <ul class="social mb-0 list-inline mt-3">
                   
                  </ul>
                </div>
              </div>
              <div class="col-xl-3 col-sm-6 mb-5">
                <div class="bg-white rounded shadow-sm py-5 px-4"><img src="" alt="" width="100" class="img-fluid rounded-circle mb-3 img-thumbnail shadow-sm"/>
                  <h5 class="mb-0">Pune Hadapsar</h5><h6>Ph. 3333</h6><span class="small text-uppercase text-muted">Address :A1 TOWER,Magarpatta south-gate,Hadapsar, Pune : 415001 </span>
                  <ul class="social mb-0 list-inline mt-3">
                    
                  </ul>
                </div>
              </div>
              <h2 class="font-weight-light"><b>Foodies Team Are Available 24/7</b> </h2>
            </div>
          </div>
        </div>
       
        <footer class="bg-light pb-5">
          <div class="container text-center">
            <p class="font-italic text-muted mb-0">&copy; Copyrights Foodies.com All rights reserved.</p>
          </div>
        </footer>
        </section>
    )
}
export default Contact;