package com.sunbeam.serviceImpl;


import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunbeam.daos.AddressDao;
import com.sunbeam.daos.UsersDao;
import com.sunbeam.entities.Address;
import com.sunbeam.entities.Users;
import com.sunbeam.services.AddressService;


@Service
@Transactional
public class AddressServiceImpl implements AddressService{
	@Autowired
	private AddressDao addressDao;
	@Autowired
	private UsersDao userDao;

	@Override
	public Address addAddress(Address address, int userId) {
		Optional<Users> user = userDao.findById(userId);
		if(user.isPresent()) {
			user.get().addAddress(address);
			return address;
		}
		return null;
	}

	@Override
	public Address editAddress(Address address, int addressId) {
		Optional<Address> address1 = addressDao.findById(addressId);
		Address address2 = address1.orElse(null);
		address2.setAddressLine1(address.getAddressLine1());
		address2.setAddressLine2(address.getAddressLine2());
		System.out.println("------------------------"+address2);
		return address2;
	}

	@Override
	public String deleteAddress(int addressId) {
		System.out.println("addressId : "+ addressId);
		addressDao.deleteById(addressId);
		return "success";
	}

	@Override
	public List<Address> getAllAddressesByUserId(int userId) {
		
		return addressDao.getAllAddressesByUserId(userId);
	}

}
