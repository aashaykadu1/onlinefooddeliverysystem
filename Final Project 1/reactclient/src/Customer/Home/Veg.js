import axios from 'axios'
import { useState, useEffect } from 'react'
// import { url } from '../../Constants/Url'
import MenuTypeRow from './MenuTypeRow'
import Navbar2 from '../../Components/C_navbar'
import './customerHome.css'
import './Common.css';
const Veg = () => {
  const [menuTypeId,setMenuTypeId]=useState(1)
  const [menutype, setMenuType] = useState([])

  useEffect(() => {
    console.log(`menutype is loaded`)
    getMenutype()
  }, [])

  const getMenutype = () => {
    axios.get('http://localhost:8080' + `/menutype/allMenuByType/${menuTypeId}`).then((response) => {
      const result = response.data
      if (result.status === 'success') {
        setMenuType(result.data)
      } else {
        alert('error while loading list of menutype')
      }
    })
  }

  return (

    <section class="pb-5 header text-center" >
                  <Navbar2/>
    <div class="container py-5 text-white"  className="RowStyle" >
  
        <div class="row" >
            <div class="col-lg-9 mx-auto">
                <div class="card border-0 shadow">
                    <div >

                        
                        <div>
                            <table class="table m-0" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">Menu_Id</th>
                                        <th scope="col">Menu Name</th>
                                        <th scope="col">price</th>
                                        <th scope="col">Image</th>
                                        <th scope="col">Description</th>
                            
                                    </tr>
                                </thead>
                                <tbody >
                                {menutype.map((menutype) => {
                                   return <MenuTypeRow menutype={menutype} />
                                 })} 
                                </tbody>
                            </table>
                            {/* <a href="/HotelMenu" className="btn btn-info">Back</a> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
  )
}

export default Veg;

// import React from "react";
// import Navbar2 from "../../Components/C_navbar";
// import { Link } from "react-router-dom";
// import kajucurry from '../Images/Veg/Kajucurry.jpg'
// import Panner from '../Images/Veg/Paneer.jfif'
// import PannerMasala from '../Images/Veg/PannerMasala.jfif'
// import VegAngara from '../Images/Veg/Vegangara.jfif'
// import VegMaratha from '../Images/Veg/Vegmaratha.jpg'

// const Veg=()=>{
//     return(
//         <div className="container">
//        <Navbar2></Navbar2>
//         <div className="container">
        
//         <div className="container">

//             <table>
//                 <tr>
//                     <td>
//                         <div className=" anchordiv" >
//                             <Link className="nav-link" to="/addcart">
//                                 <div>
//                                     <img src={kajucurry} className="anchorimages" />
//                                 </div>
//                                 <h6>kajucurry</h6>
//                             </Link>
//                         </div>
//                     </td>

//                     <td>
//                         <div className="anchordiv">
//                             <Link className="nav-link" to="/addcart">
//                                 <div>
//                                     <img src={Panner} className="anchorimages" />
//                                 </div>
//                                 <h6>Panner</h6>
//                             </Link>
//                         </div>
//                     </td>
//                     <td>
//                         <div className="anchordiv">
//                             <Link className="nav-link" to="/addcart">
//                                 <div>
//                                     <img src={PannerMasala} className="anchorimages" />
//                                 </div>
//                                 <h6>PannerMasala</h6>
//                             </Link>
//                         </div>
//                     </td>
//                 </tr>
//                 <tr>
//                     <td>
//                         <div className=" anchordiv" >
//                             <Link className="nav-link" to="/signin">
//                                 <div>
//                                     <img src={VegAngara} className="anchorimages" />
//                                 </div>
//                                 <h6>VegAngara</h6>
//                             </Link>
//                         </div>
//                     </td>

//                     <td>
//                         <div className="anchordiv">
//                             <Link className="nav-link" to="/signin">
//                                 <div>
//                                     <img src={VegMaratha} className="anchorimages" />
//                                 </div>
//                                 <h6>VegMaratha</h6>
//                             </Link>
//                         </div>
//                     </td>
                   
//                 </tr>
//             </table>

//         </div>
//     </div>

//  </div >
//     )
// }
// export default Veg;