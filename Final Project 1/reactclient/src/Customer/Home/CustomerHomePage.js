// import Navbar2 from "../../Components/C_navbar"
import '../Home/customerHome.css'
import { Link } from "react-router-dom"
import biryani from '../images/biryani.jpg'
import chineese from '../images/chinees.jfif'
import maharastrian from '../images/maharastrian_thali.jfif'
import mexican from '../images/Mexican.jfif'
import nonveg from '../images/Nonveg.jpg'
import southIndian from '../images/south-indian.jpg'
import sweets from '../images/Sweets.jfif'
import veg from '../images/Veg.jfif'
import CustHomeNv from '../../Components/CustHomeNv'
const CustomerHome=()=>{
    return(
        
        <div className="container">

            <CustHomeNv></CustHomeNv>

            {/* <Navbar2></Navbar2> */}
            <div className="container">
            
            <div className="container">

                <table>
                    <tr>
                        <td>
                            <div className=" anchordiv" >
                                <Link className="nav-link" to="/Veg1">
                                    <div>
                                        <img src={veg} className="anchorimages" />
                                    </div>
                                    <h6>vegetarian</h6>
                                </Link>
                            </div>
                        </td>

                        <td>
                            <div className="anchordiv">
                                <Link className="nav-link" to="/Thalis">
                                    <div>
                                        <img src={maharastrian} className="anchorimages" />
                                    </div>
                                    <h6>Thalis</h6>
                                </Link>
                            </div>
                        </td>
                        <td>
                            <div className="anchordiv">
                                <Link className="nav-link" to="/SouthIndian">
                                    <div>
                                        <img src={southIndian} className="anchorimages" />
                                    </div>
                                    <h6>southindian</h6>
                                </Link>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div className=" anchordiv" >
                                <Link className="nav-link" to="/Chineese">
                                    <div>
                                        <img src={chineese} className="anchorimages" />
                                    </div>
                                    <h6>Chineese</h6>
                                </Link>
                            </div>
                        </td>

                        {/* <td>
                            <div className="anchordiv">
                                <Link className="nav-link" to="/signin">
                                    <div>
                                        <img src={mexican} className="Mexican" />
                                    </div>
                                    <h6>Maxican</h6>
                                </Link>
                            </div>
                        </td> */}
                        <td>
                            <div className="anchordiv">
                                <Link className="nav-link" to="/Italian">
                                    <div>
                                        <img src={nonveg} className="anchorimages" />
                                    </div>
                                    <h6>Italian</h6>
                                </Link>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div className=" anchordiv" >
                                <Link className="nav-link" to="/Nonveg">
                                    <div>
                                        <img src={biryani} className="anchorimages" />
                                    </div>
                                    <h6>Non-veg Special</h6>
                                </Link>
                            </div>
                        </td>

                        <td>
                            <div className="anchordiv">
                                <Link className="nav-link" to="/Desserts">
                                    <div>
                                        <img src={sweets} className="anchorimages" />
                                    </div>
                                    <h6>Desserts</h6>
                                </Link>
                            </div>
                        </td>
                    </tr>
                </table>

            </div>
        </div>

     </div >

    )
}

export default CustomerHome;









// import axios from 'axios'
// import { useState, useEffect } from 'react'
// // import { url } from '../../Constants/Url'
// import HorizontalSlider from '../components/HorizontalSlider'
// // import { useHistory } from 'react-router-dom'

// const CustomerHome = () => {
//   const [menu, setMenu] = useState([])
  

  
//   useEffect(() => {
//     getMenu()
//   }, [])

  

//   const getMenu = () => {
   
//     axios.get('http://localhost:8080/menu/all').then((response) => {
//       const result = response.data
//       if (result.status === 'success') {
//         setMenu(
//           result.data.map((menu) => {
//             return {
//               ...menu,
//               title: `${menu.menuName}`,
//             }
//           })
//         )
//       } else {
//         alert('error occured while getting all menu')
//       }
//     })
//   }


//   return (
//     <div>
//       <h2 className="page-title">Home</h2>

//       <HorizontalSlider
        
//         items={artists}
//         title="Featured Artists"
//       />
//     </div>
//   )
// }

// export default CustomerHome