package com.sunbeam.controllers;



import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sunbeam.dtos.UserDTO;
import com.sunbeam.dtos.ResponseDto;
import com.sunbeam.dtos.UserDTO;
import com.sunbeam.entities.Users;
import com.sunbeam.services.UserService;

@CrossOrigin
@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private UserService userService;
	

	@GetMapping("/userList")
	public ResponseEntity<?>findAllUsers(){
		List<Users> userlist=userService.getAllUser("customer");
		List<UserDTO> result=new ArrayList<UserDTO>();
		for (Users user : userlist) 
			result.add(UserDTO.fromEntity(user));
		return ResponseDto.success(result);
		
	}
	
	
	@GetMapping("/deliveryBoyList")
	public ResponseEntity<?>findAllDeliveryBoy(){
		List<Users> userlist=userService.getAllDeliveryBoy("deliveryBoy");
		List<UserDTO> result=new ArrayList<UserDTO>();
		for (Users user : userlist) 
			result.add(UserDTO.fromEntity(user));
		return ResponseDto.success(result);
		
	}
}
