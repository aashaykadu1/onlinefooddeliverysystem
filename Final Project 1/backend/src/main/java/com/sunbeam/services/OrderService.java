package com.sunbeam.services;


import java.util.List;

import com.sunbeam.dtos.OrderResponse;
import com.sunbeam.entities.Order;

public interface OrderService {

	String placeOrderForUser(int userId, int addrId, String paymentMode);

	List<OrderResponse> getAllOrders();
	
	List<OrderResponse> getAllPendingOrders();

	List<OrderResponse> getAllAssignedOrders(int userId);

	List<OrderResponse> getAllCustomerOrders(int userId);

	void assignDeliveryBoy(int userId, int orderId);

	void updateOrderStatus(int orderId, String string,int deliveryId);
	
	List<OrderResponse> getMyOrders(int userId);
	
	//new try
	



}
