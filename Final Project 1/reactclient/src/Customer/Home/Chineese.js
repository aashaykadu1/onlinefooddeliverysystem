import axios from 'axios'
import { useState, useEffect } from 'react'
// import { url } from '../../Constants/Url'
// import MenuTypeRow from './MenuTypeRow'
import Navbar2 from '../../Components/C_navbar'
import MenuTypeRow1 from './MenuTypeRow1'
import { Row, Col } from "react-bootstrap";

const Chineese = () => {
  const [menuTypeId,setMenuTypeId]=useState(3)
  const [menutype, setMenuType] = useState([])

  useEffect(() => {
    console.log(`menutype is loaded`)
    getMenutype()
  }, [])

  const getMenutype = () => {
    axios.get('http://localhost:8080' + `/menutype/allMenuByType/${menuTypeId}`).then((response) => {
      const result = response.data
      if (result.status === 'success') {
        setMenuType(result.data)
      } else {
        alert('error while loading list of menutype')
      }
    })
  }

  return (

    <>
    <Navbar2/>
    <Row>
              {menutype.map((menutype) => (
                <Col key={menutype.menuTypeId} md={3}>
                  <MenuTypeRow1 menutype={menutype} />
                </Col>
              ))}
            </Row>
                 
    
               
    </>
  )
}

export default Chineese;
// import React from "react";
// import Navbar2 from "../../Components/C_navbar";
// import { Link } from "react-router-dom";
// import lollipop from '../Images/Chinnes/lollipop.jfif'
// import noodles from '../Images/Chinnes/noodles.jfif'
// import soup from '../Images/Chinnes/soup.jfif'
// import triplerice from '../Images/Chinnes/tripleRice.jpg'

// const Chineese=()=>{
//     return(
//         <div className="container">
//            <Navbar2></Navbar2>
//             <div className="container">
            
//             <div className="container">

//                 <table>
//                     <tr>
//                         <td>
//                             <div className=" anchordiv" >
//                                 <Link className="nav-link" to="/signin">
//                                     <div>
//                                         <img src={lollipop} className="anchorimages" />
//                                     </div>
//                                     <h6>lollipop</h6>
//                                 </Link>
//                             </div>
//                         </td>

//                         <td>
//                             <div className="anchordiv">
//                                 <Link className="nav-link" to="/signin">
//                                     <div>
//                                         <img src={noodles} className="anchorimages" />
//                                     </div>
//                                     <h6>noodles</h6>
//                                 </Link>
//                             </div>
//                         </td>
//                         <td>
//                             <div className="anchordiv">
//                                 <Link className="nav-link" to="/signin">
//                                     <div>
//                                         <img src={soup} className="anchorimages" />
//                                     </div>
//                                     <h6>soup</h6>
//                                 </Link>
//                             </div>
//                         </td>
//                     </tr>
//                     <tr>
//                         <td>
//                             <div className=" anchordiv" >
//                                 <Link className="nav-link" to="/signin">
//                                     <div>
//                                         <img src={triplerice} className="anchorimages" />
//                                     </div>
//                                     <h6>triplerice</h6>
//                                 </Link>
//                             </div>
//                         </td>

                        
//                     </tr>
//                 </table>

//             </div>
//         </div>

//      </div >
//     )
// }
// export default Chineese;