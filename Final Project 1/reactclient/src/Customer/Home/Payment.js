import { Link, useNavigate } from "react-router-dom"
import { useEffect,useState } from "react";
// import { useHistory } from "react-router";
import axios from "axios";
import { toast } from "react-toastify";
// import {url} from '../../Constants/Url'

const Payment=()=>{
  
        // const history = useHistory();
        const [userid,setUserId]=useState(sessionStorage.getItem("userId"))
        const [addressId, setAddressId] = useState(sessionStorage.getItem("addressId"))
        const [paymentMode, setPaymentMode] = useState('')
        const [order]=useState([])
        const[address]=useState([])

        
   const navigate = useNavigate()

        const [addresse , setAddresse] = useState([])
    useEffect(() => {
      console.log(`User is loaded`)
      getAddresse()
     
    }, [])

        const getAddresse = () => {
            
            axios.get('http://localhost:8080' + `/address/find/${userid}`).then((response) => {
               
            // axios.get(url + `/address/all/${userid}`).then((response) => {
              const result = response.data
              console.log(result.data.addressId)
              console.log(result)
              sessionStorage.setItem("addressId",result.data.id);
              if (result.status === 'success') {
                setAddressId(result.data.id)
              } else {
                alert('error while loading list of users')
              }
            })
          }
        
        useEffect(() => {
          // let id=sessionStorage.getItem("addressId");
          // let id1=sessionStorage.getItem("userId");
          //   setAddressId(id);
          // setUserId(id1);  
        }, [])
        
        const setPayment = () => {
            if (addressId.length === 0) {
                alert('please enter addressId')
            } else if (paymentMode.length === 0) {
                alert('Enter new paymentMode')
            } else {
                const body = new FormData()
                body.append('addressId', addressId)
               
                body.append('paymentMode', paymentMode)
                body.append('userId', userid)
             
    
                axios.post('http://localhost:8080/order/place',{'addressId':addressId,'paymentMode':paymentMode,'userId':userid} , {"Content-Type": "application/json"}).then((response) => {
                   
                   console.log(response);
                    const result = response.data
                    // console.log(result.data.addressId)
                    // console.log(result)
                   
                    // sessionStorage.setItem("userId" , result.data.id)
                    // sessionStorage.setItem("addressId",result.data.id);
                    
                    if (result.status === 'success') {
                        
                        toast.success("Your payment Added Successfully !!!")
        
                           navigate('/VisitAgain')       
                    } else {
                        toast.warning('payment Failed')
                    }
                }
                )
            }
    
    
        }
        return(
            <section className="vh-100" >
            <div className="container py-5 h-100">
              <div className="row d-flex justify-content-center align-items-center h-100">
                <div className="col col-xl-10">
                <div class="col-md-6 text-left text-white lcol">
                 <div class="greeting">
                     <h3>Happy To See U In <span class="txt">Foodies</span></h3>
                </div>
                 <h6 class="mt-3">let's Make a Payment in Simpler Way...</h6>
                <div class="footer">
                    <div class="socials d-flex flex-row justify-content-between text-white">
                        <div class="d-flex flex-row"><i class="fab fa-linkedin-in"></i><i class="fab fa-facebook-f"></i><i class="fab fa-twitter"></i></div> 
                    </div>
                 </div>
             </div>
                  <div className="card">
                    <div className="row g-0">
                      <div className="flex align-items-center">
                        <div className="card-body p-4 p-lg-5 text-black">
                        <div class="row"> 
                          <div class="icons"> 
                                <img src="https://img.icons8.com/color/48/000000/visa.png" /> 
                                <img src="https://img.icons8.com/color/48/000000/mastercard-logo.png" />
                               <img src="https://img.icons8.com/color/48/000000/maestro.png" />
                           </div>
                          </div>
                          <form class="mx-1 mx-md-4">
                            <h5 className="fw-normal mb-3 pb-3">Payment Here</h5>
          
                             <div className="form-outline mb-4">
                             
                            </div> 
                           
                           
                           <div className="form-outline mb-4">
                              <input type="text" id="form2Example27" class="form-control form-control-lg" value={userid}
                               onChange={(e)=>{
                               setUserId(e.target.value);
                                }}/>
                              <label className="form-label" for="form2Example27" 
                              >userId</label>
                            </div>

                            <div className="form-outline mb-4">
                              <input type="text" id="form2Example27" class="form-control form-control-lg" value={addressId}
                                onChange={(e) => {
                                    setAddressId(e.target.value)
                                   }}
                                  
                                  />
                              <label className="form-label" for="form2Example27" 
                              >addressId</label>
                            </div>

                {/* <div class="col-auto">
                  <label for="inputPassword2" class="visually-hidden">Address Id</label>
                  <input type="text" class="form-control" id="inputPassword2" placeholder="AddressId"
                   onChange={(e) => {
                    setAddressId(e.target.value)
                   }}
                  value={addressId}
                  />
                </div>
                  */}
                           { /* <div className="form-outline mb-4">
                              <input type="text" id="form2Example27" class="form-control form-control-lg" 
                               onChange={(e)=>{
                               setPaymentMode(e.target.value);
                                }}/>
                              <label className="form-label" for="form2Example27" 
                              >paymentMode</label>
                              </div> */}
                           

                            <label className="btn btn-info"> Payment Type </label>  
                    <select
                     onChange={(e) => {
                        setPaymentMode(e.target.value)
                        }}
                     >   <option value = ""> paymentMode </option>  
                         <option value = "COD"> COD </option>  
                         <option value = "CARD"> Card </option>  
                         <option value = "UPI"> UPI </option>  
                         <option value = "NETBANKING"> Net Banking </option>  
                    </select>  
          
                            <div className="pt-1 mb-4">
                              <button onClick={setPayment} className="btn btn-success" type="button">Save</button>
                            </div>
                          </form>
          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        )



}
export default Payment;



















// const Payment=()=>{

//     let history = useHistory();
//     const [userid,setUserId]=useState(sessionStorage.getItem("userId"))
//     // const [userId,setUserId] = useState('')
//     const [addressId, setAddressId] = useState(sessionStorage.getItem("addressId"))
//     const [paymentMode,setPaymentMode]=useState('')
//     const [payments]=useState([])
//     const [addresses]=useState([])

//     const [addresse , setAddresse] = useState([])
//     useEffect(() => {
//       console.log(`User is loaded`)
//       getAddresse()
     
//     }, [])

//         const getAddresse = () => {
            
//             axios.get(url + `/address/find/${userid}`).then((response) => {
               
//             axios.get(url + `/address/all/${userid}`).then((response) => {
//               const result = response.data
//               console.log(result.data.addressId)
//               console.log(result)
//               sessionStorage.setItem("addressId",result.data.id);
//               if (result.status === 'success') {
//                 setAddressId(result.data.id)
//               } else {
//                 alert('error while loading list of users')
//               }
//             })
//           }

//           const orderPlaced = () => {  

                        
//                 const body = new FormData()
//                 body.append('userId', userid)
               
//                 body.append('addressId', addressId)
//                 body.append('paymentMode',paymentMode)
             
    
//                 axios.post('http://localhost:8080/order/place',{'userId':userid,'addressId':addressId,'paymentMode':paymentMode} , {"Content-Type": "application/json"}).then((response) => {
                   
    
//                    console.log(response);
//                     const result = response.data
//                     sessionStorage.setItem("userId" , result.data.id)
                   
//                 }
//                 )
            
    
    
//         }

   
 
    
    

   
    

    



//     return(

    


//         <div class="container d-flex justify-content-center">
            
//          <div class="row my-5">
//              <div class="col-md-6 text-left text-white lcol">
//                 <div class="greeting">
//                      <h3>Happy To See U In <span class="txt">Foodies</span></h3>
//                  </div>
//                  <h6 class="mt-3">let's Make a Payment in Simpler Way...</h6>
//                  <div class="footer">
//                      <div class="socials d-flex flex-row justify-content-between text-white">
//                          <div class="d-flex flex-row"><i class="fab fa-linkedin-in"></i><i class="fab fa-facebook-f"></i><i class="fab fa-twitter"></i></div> 
//                      </div>
//                  </div>
//              </div>
//              <div class="col-md-6 rcol">
//              <div class="row"> 
//                           <div class="icons"> 
//                                 <img src="https://img.icons8.com/color/48/000000/visa.png" /> 
//                                 <img src="https://img.icons8.com/color/48/000000/mastercard-logo.png" />
//                                 <img src="https://img.icons8.com/color/48/000000/maestro.png" />
//                            </div>
//                           </div>
//                  <form class="sign-up">
//                      <h2 class="heading mb-4">Payment</h2>
          

//                 <div class="col-auto">
//                   <label for="inputPassword2" class="visually-hidden">User_Id</label>
//                    <input type="text" class="form-control" id="inputPassword2" placeholder="UserId" value={sessionStorage.getItem("userId")}
//                    onChange={(e) => {
//                     setUserId(e.target.value)
//                    }}
//                   />
//                 </div>
//                 <div class="col-auto">
//                   <label for="inputPassword2" class="visually-hidden">Address Id</label>
//                   <input type="text" class="form-control" id="inputPassword2" placeholder="AddressId"
//                    onChange={(e) => {
//                     setAddressId(e.target.value)
//                    }}
//                   value={addressId}
//                   />
//                 </div>
               
                    
//                      {/* <div class="form-group fone mt-2"> <i class="fas fa-lock"></i> <input type="text" class="form-control" placeholder="Payment Type" /> </div> */}
//                      <label className="btn btn-info"> Payment Type </label>  
//                      <select
//                     onChange={(e) => {
//                         setPaymentMode(e.target.value)
//                        }}
//                     >  
//                         <option value = "COD"> COD </option>  
//                         <option value = "CARD"> Card </option>  
//                         <option value = "UPI"> UPI </option>  
//                         <option value = "NETBANKING"> Net Banking </option>  
//                      </select>  
                    
                       
                  
//                  </form> 
                
//                  {/* <Link to=" " onClick={orderPlaced} class="btn btn-success mt-5">Payment Confirm
//                  </Link>
//                   */}
                
//              </div>
//          </div>
//      </div>



//      )
// }

// export default Payment