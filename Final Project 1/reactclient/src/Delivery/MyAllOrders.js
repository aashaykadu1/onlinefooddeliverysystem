import { Link } from "react-router-dom"
import axios from 'axios'
import { useState, useEffect } from 'react'
// import { url } from '../../Constants/Url'
import OrdersRow from "../Admin/Home/OrdersRow1"
import Navbar4 from "../Components/D_navbar"
const MyAllOrders=()=>{
    const [orders, setOrders] = useState([])
    const [userid]=useState(sessionStorage.getItem('userId'))
    
    useEffect(() => {
      // console.log(`orders is loaded`)
      getOrders()
    }, [])
  
    const getOrders = () => {
        console.log(userid)
      axios.get('http://localhost:8080' + `/order/assigned/${userid}`).then((response) => {
        const result = response.data
        // sessionStorage.setItem("orderId",result.data.orderId);
        // console.log(result.data[0].order.id)
        if (result.status === 'success') {
         
          setOrders(result.data)
           console.log(result.order)
        } else {
          alert('error while loading list of orders')
        }
      })
    }
  
    return (
  
      <section class="pb-5 header text-center">
                <Navbar4></Navbar4>
      <div class="container py-5 text-white">
    
          <div class="row">
              <div class="col-lg-9 mx-auto">
                  <div class="card border-0 shadow">
                      <div >
  
                          
                          <div >
                              <table class="table m-0" className="table table-hover">
                                  <thead>
                                      <tr>
                                          {/* <th scope="col">User_Id</th> */}
                                          <th scope="col">Order_Id</th>
                                          {/* <th scope="col">Menu_Name</th> */}
                                          {/* <th scope="col">Qty</th> */}
                                         
                                          <th scope="col">Customer Name</th>
                                          <th scope="col">Address</th>
                                          <th scope="col">Grand Total</th>
                                          <th scope="col">Order Status</th>
                                          <th scope="col">Payment Status</th>
                              
                                      </tr>
                                  </thead>
                                  <tbody>
                                  {orders.map((order) => {
                                    // sessionStorage.setItem("orderId",order.order.id)
                                     
                                     return <OrdersRow order={order} />
                                   })} 
                                  </tbody>
                              </table>
                              <a href="/CustomerHomePage" className="btn btn-info">Back</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>

    )
}
export default MyAllOrders;