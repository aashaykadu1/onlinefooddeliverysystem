import { Link } from 'react-router-dom'

import React from 'react'

const DeliveryBoyRow1 = ({ user }) => {
  return (
    <tr>
    <td>{user.id}</td>
    <td>{user.firstName}</td>
    <td> {user.lastName}</td>
    <td>{user.email}</td>
    <td>{user.phoneNo}</td>
  
  </tr>
  )
}

export default DeliveryBoyRow1
