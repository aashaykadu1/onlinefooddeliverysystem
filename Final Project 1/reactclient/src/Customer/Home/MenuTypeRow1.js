import React from 'react'
import { Card, Button } from "react-bootstrap";

  // import { Link } from "react-router-dom";
const MenuTypeRow1 = ({menutype}) => {

    const addToCart=(id)=> {
        sessionStorage.setItem("menuId",id);
      }
  return (
    <>
      
      

      <Card className="my-3 p-3 rounded" style={{ width: '18rem', margin: '50px' }}>
      {/* <Link to={`/menutype/${menutype.menuId}`}> */}
  <Card.Img variant="top" src="https://www.bing.com/th?id=OIP.R0bZc5_hnQ3YKtbL4wrJHwHaJO&w=172&h=214&c=8&rs=1&qlt=90&o=6&dpr=1.25&pid=3.1&rm=2" />
  {/* </Link> */}
  <Card.Body>
    <Card.Title>
        <h4>{menutype.menuName}</h4>
    </Card.Title>
    <Card.Text>
    <h6>{menutype.description}</h6>
    </Card.Text>
    <Card.Text>
    Rs. {menutype.price}
    </Card.Text>
   
   {/* <Link to="/home">
   
   
    <Button variant="primary">Add to Cart</Button>
    </Link> */}


<Card.Link href="/AddCart"

  to={{ state: {menutypeid: menutype.id }}}
 onClick={() => addToCart(menutype.id)}
>
<Button variant="primary">Add to Cart</Button>
</Card.Link>

  </Card.Body>
</Card>












      {/* <Card className="my-3 p-3 rounded">
        <Link to={`/menutype/${menutype.id}`}>
          <Card.Img src={menutype.image} variant="top" />
        </Link>
        <Card.Body>
          <Link to={`/menutype/${menutype.id}`}>
            <Card.Title as="div">
              <strong>{menutype.menuName}</strong>
            </Card.Title>
          </Link>
          <Link to={`/menutype/${menutype.id}`}>
            <Card.Title as="div">
              <strong>{menutype.description}</strong>
            </Card.Title>
          </Link>
          <Card.Text as="div">
            <Rating
              value={menutype.rating}
              text={`${menutype.numReviews} reviews`}
            />
          </Card.Text>
          <Card.Text as="div">Rs {menutype.price}</Card.Text>
        </Card.Body>
      </Card>
 */}






    </>
  )
}

export default MenuTypeRow1
