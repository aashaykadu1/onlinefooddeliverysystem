package com.sunbeam.serviceImpl;



import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sunbeam.daos.MenuDao;
import com.sunbeam.daos.Menu_TypeDao;
import com.sunbeam.dtos.MenuDto;
import com.sunbeam.entities.Menu;
import com.sunbeam.entities.MenuType;
import com.sunbeam.services.MenuService;
//import com.sunbeam.util.StorageService;
import com.sunbeam.util.StorageService;

@Transactional
@Service
public class MenuServiceImpl implements MenuService {
	@Autowired
	private MenuDao menuDao;
	
	@Autowired
	private StorageService storageService;
	
	@Autowired
	private Menu_TypeDao menuTypeDao;
	
	
//	@Override
//	public String addMenu(Menu menu) {
//		//Optional<MenuType> menuType = menuTypeDao.findById(menudto.getMenuType());
//		//Menu menu = menudto.getMenu();
//		menuDao.save(menu);
//		return "success";
//	}
	@Override
	public List<Menu> findAll() {
			List<Menu> menuList = menuDao.findAll();
			return menuList;
		}
	@Override
	public String deleteMenu(int id) {
		menuDao.deleteById(id);
		return "Menu Deleted successfully";
	}
	@Override
	public Menu editMenu(MenuDto menuDto , int id) {
		Menu menu = menuDao.getById(id);
//		System.out.println(menu);
		menu.setDescription(menuDto.getDescription());
		menu.setPrice(menuDto.getPrice());
		return menuDao.save(menu);
		
//		return menu;
	}
	@Override
	public Menu saveMenu(Menu menu, MultipartFile imageName) {
		String fileName = storageService.store(imageName);
		menu.setImageName(fileName);
		return menuDao.save(menu);
	}
	//practice
//	public Menu saveMenu(Menu menu) {
//		return menuDao.save(menu);
//	}
	@Override
	public Menu addMenu(Menu menu, MultipartFile imageName , int menuTypeid) {
		String image = storageService.store(imageName);
		menu.setImageName(image);
		Optional<MenuType> menuType = menuTypeDao.findById(menuTypeid);
		if(menuType.isPresent()) {
			menuType.get().addMenu(menu);
			return menu;
		}
		
		return menuDao.save(menu);
	}
	


}
